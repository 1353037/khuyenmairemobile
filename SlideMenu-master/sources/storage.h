#ifndef STORAGE_H
#define STORAGE_H
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlDriver>
#include <QDebug>
#include <QtScript/QScriptEngine>
#include <QtQuick>
#include <QObject>
#include <QString>
class Storage : public QObject
{
    Q_OBJECT
public:
    Q_INVOKABLE int getUserID() {
        QSettings settings;
        return settings.value("KhuyenmaiRe/userID", -1).toInt();
    }
    Q_INVOKABLE QString getProvinceID() {
        QSettings settings;
        return settings.value("KhuyenmaiRe/provinceID", "all").toString();
    }
    Q_INVOKABLE QString getProvinceName() {
        QSettings settings;
        return settings.value("KhuyenmaiRe/provinceName", "Tất cả").toString();
    }
    Q_INVOKABLE int getOTP() {
        QSettings settings;
        return settings.value("KhuyenmaiRe/OTP", 0).toInt();
    }
    Q_INVOKABLE QDateTime getOTPDate() {
        QSettings settings;
        return settings.value("KhuyenmaiRe/OTPDate", "").toDateTime();
    }

    Q_INVOKABLE void updateUserID(int user_id) {
        QSettings settings;
        settings.setValue("KhuyenmaiRe/userID", user_id);
        settings.sync();
    }
    Q_INVOKABLE void updateProvinceID(QString s) {
        QSettings settings;
        settings.setValue("KhuyenmaiRe/provinceID", s);
        settings.sync();
    }
    Q_INVOKABLE void updateProvinceName(QString s) {
        QSettings settings;
        settings.setValue("KhuyenmaiRe/provinceName", s);
        settings.sync();
    }
    Q_INVOKABLE void updateOTP(int s) {
        QSettings settings;
        settings.setValue("KhuyenmaiRe/OTP", s);
        settings.sync();
    }
    Q_INVOKABLE void updateOTPDate(QDateTime s) {
        QSettings settings;
        settings.setValue("KhuyenmaiRe/OTPDate", s);
        settings.sync();
    }

public slots:
};

#endif // STORAGE_H
