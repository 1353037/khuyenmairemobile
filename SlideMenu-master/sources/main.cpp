#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml>
#include <QtSql/QSqlDatabase>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlDriver>
#include <QDebug>
#include <QtScript/QScriptEngine>
#include <QtQuick>
#include <QObject>
//local.dieuviet.com
//khuyenmaire/KMR@123

#include "mydevice.h"
#include "storage.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    qmlRegisterType<MyDevice>("mydevice", 1, 0, "MyDevice");

    Storage store;
    QQmlApplicationEngine engine;
    QQmlContext* ctx = engine.rootContext();
    ctx->setContextProperty("Storage", &store);

    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    return app.exec();

}


