import QtQuick 2.4
import QtQuick.Window 2.2
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.3
import QtPositioning 5.3
import QtLocation 5.5
import QtQuick.Controls.Private 1.0
//import QtWebKit 3.0

import mydevice 1.0

ApplicationWindow {
    id: app    

    property string appTitle: ""

    width: Screen.width
    height: Screen.height
    visible: true

    property alias dp: device.dp
    property string version: "1.0"
    property string rootURL: "http://api.khuyenmai.re/" // "localhost:30208/"
    property real latitude: 0.0
    property real longitude: 0.0
    property string address: ""
    property string provinceId: Storage.getProvinceID()
    property string provinceName: Storage.getProvinceName()
    property string sortId: "new"
    property int categoryId: 0
    property int userId: Storage.getUserID()
    property int otp: Storage.getOTP()
    property date otpDate: Storage.getOTPDate()
    property int virtualUserId: 11

    property string type: "deal"

    MyDevice { id: device }


    ListModel{
        id: userInfo
    }

    String
    {
        id: string
    }
    Color
    {
        id: palette
    }



    property int orientationPortrait: 1
    property int orientationLandscape: 2
    property int orientation: 0
    /*onWidthChanged: {
        if( width > height ) {
            app.orientation = app.orientationLandscape
        } else {
            app.orientation = app.orientationPortrait
        }
    }*/

    //property string version: "2015100901"

    property alias currentPage: loader.source
    property string currentAppTitle: app.appTitle

    property int durationOfMenuAnimation: 300
    property int menuWidth: app.width * 0.8//app.orientation == app.orientationLandscape ? 300*app.dp : app.width*0.85
    property int menuHeight: 60 * app.dp
    property int searchHeight: 50 * app.dp
    property int widthOfSeizure: 15*app.dp
    property bool menuIsShown: menuView.x === -menuWidth ? false : true
    property real menuProgressOpening


    Rectangle{
        id: searchBrandsView
        z: 3
        //anchors.top: menuBar.bottom
        //anchors.topMargin: 100 * app.dp
        width: parent.width
        height: searchHeight
        //x: - app.width
        x: app.width
        y: menuHeight
        //z: 6

        Behavior on x { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        color: palette.color_bg

        MouseArea
        {
            anchors.fill: parent
        }

        focus: true
        Keys.onBackPressed: brandsView_back()

        TextField
        {
            id: searchBrandsField
            anchors.fill: parent
            anchors.margins: 10 * app.dp
            onTextChanged: updateFilterBrands(text)
            placeholderText: string.brand_search
            horizontalAlignment: TextInput.AlignHCenter
            font.pixelSize: 20 * app.dp
            maximumLength: 20

            style: TextFieldStyle {
                    textColor: palette.colorPrimary
                    background: Rectangle {
                        radius: 5
                        border.color: palette.color_bg
                        border.width: 1
                    }
                }
        }
    }

    Rectangle{
        id: searchDealsView
        z: 11
        //anchors.top: menuBar.bottom
        //anchors.topMargin: 100 * app.dp
        width: parent.width
        height: searchHeight
        x: - app.width
        y: menuHeight

        Behavior on x { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        color: palette.color_bg


        MouseArea
        {
            anchors.fill: parent
        }
        focus: true
        Keys.onBackPressed: dealsView_back()

        TextField
        {
            id: searchDealsField
            anchors.fill: parent
            anchors.margins: 10 * app.dp
            onTextChanged: updateFilterDeals(text)
            placeholderText: string.deal_search
            horizontalAlignment: TextInput.AlignHCenter
            font.pixelSize: 20 * app.dp
            maximumLength: 20

            style: TextFieldStyle {
                    textColor: palette.colorPrimary
                    background: Rectangle {
                        radius: 5
                        border.color: palette.color_bg
                        border.width: 1
                    }
                }
        }
   }

    Rectangle {
        id: brandsView
        //anchors.top: menuBar.bottom
        anchors.top: parent.top
        anchors.topMargin: menuHeight
        anchors.bottom: parent.bottom
        //height: parent.height - menuBar.height
        width: app.width
        property string queryBrands: "http://api.khuyenmai.re/api/v1/brands"

        property int index: 3
        x: app.width
        Behavior on x { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        z: 1
        MouseArea
        {
            anchors.fill: parent
        }
        focus: true
        Keys.onBackPressed: brandsView.x = -app.width
        ScrollView {
            id: scrollBrandsView
         anchors.fill: parent
         flickableItem.interactive: true

         style: ScrollViewStyle {
                 transientScrollBars: true
                 handle: Item {
                     implicitWidth: 30
                     implicitHeight: 200
                     Rectangle {
                         color: palette.color_bg
                         anchors.fill: parent
                         anchors.topMargin: searchHeight + 6
                         anchors.leftMargin: 4
                         anchors.rightMargin: 4
                         anchors.bottomMargin: 6
                     }
                 }
                 scrollBarBackground: Item {
                     implicitWidth: 10
                     implicitHeight: 200
                 }
             }
            ListView {
                property string nextUrl: null
                property bool canAppend: true

                anchors.top: parent.top
                anchors.topMargin: searchHeight
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom

                id: listViewBrands
                delegate: brandsComponent
                clip: true
                model: ListModel{}
                onContentYChanged:
                {
                    if(scrollBrandsView.flickableItem.visibleArea.yPosition > 0.5){
                        if(nextUrl !== null)
                            request(listViewBrands, nextUrl)
                    }
                }
                property alias isEmpty: listViewBrands_error.opa
                ErrorComponent
                {
                    id: listViewBrands_error
                    property string text: string.brand_text
                }
              }
            Component.onCompleted: {
                    brandsView.queryBrands = app.rootURL + "api/v1/brand/"
                    request(listViewBrands, brandsView.queryBrands)
               }
        }
    }
    Rectangle {
        id: normalView
        x: 0
        y: 0
        width: parent.width
        height: parent.height
        color: palette.secondary
        focus: true
        Keys.onBackPressed: {  }
        property string queryNormal: "http://api.khuyenmai.re/api/v1/"+ app.type + "/?province_id=" + provinceId + "&category_id=" + categoryId + "&sort=" + sortId
      //  var _query = app.rootURL + "api/v1/" +

        Rectangle{
            id: dealsView
            width: parent.width
            height: parent.height
            x: - app.width
            y: 0
            z: 11

            color: palette.color_bg
            property string queryDeals: "http://api.khuyenmai.re/api/v1/deal"

            Behavior on x { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

            MouseArea
            {
                anchors.fill: parent
            }

            focus: true
            Keys.onBackPressed: dealsView_back()
            Rectangle {
                id: dealsView_menuBar
                z: 5
                anchors.top: parent.top
                width: parent.width
                height: app.orientation == app.orientationLandscape ? 40*app.dp : menuHeight
                color: palette.colorPrimary
                Rectangle {
                    id: dealsView_menuButton
                    anchors.left: parent.left
                    anchors.verticalCenter: parent.verticalCenter
                    width: 1.2*height
                    height: parent.height
                    scale: dealsView_maMenuBar.pressed ? 1.2 : 1
                    color: "transparent"
                    Image {
                        anchors.centerIn: parent
                        height: parent.height * 0.5
                        fillMode: Image.PreserveAspectFit
                        source: "qrc:/images/ic_back.png"
                    }
                    MouseArea {
                        id: dealsView_maMenuBar
                        anchors.fill: parent
                        onClicked: dealsView_back()
                    }
                    clip: true
                }
                Label {
                    id: dealsView_titleText
                    anchors.left: dealsView_menuButton.right
                    anchors.leftMargin: - 10 * app.dp
                    anchors.rightMargin: 0.2 * parent.height
                    anchors.verticalCenter: parent.verticalCenter
                    text: string.app_name
                    font.pixelSize: 0.25 * parent.height
                    color: palette.colorWhite
                }
            }

            Image {
                anchors.top: dealsView_menuBar.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                height: 6*app.dp
                z: 4
                source: "qrc:/images/shadow_title.png"
            }
            Loader
            {
                anchors.top: dealsView_menuBar.bottom
                anchors.topMargin: searchHeight * 2
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom

                ScrollView {
                    id: scrollDealsView
                 anchors.fill: parent
                 flickableItem.interactive: true
                 style: ScrollViewStyle {
                         transientScrollBars: true
                         handle: Item {
                             implicitWidth: 30
                             implicitHeight: 200
                             Rectangle {
                                 color: palette.color_bg
                                 anchors.fill: parent
                                 anchors.topMargin: 6
                                 anchors.leftMargin: 4
                                 anchors.rightMargin: 4
                                 anchors.bottomMargin: 6
                             }
                         }
                         scrollBarBackground: Item {
                             implicitWidth: 10
                             implicitHeight: 200
                         }
                     }
                    ListView
                    {
                        property string nextUrl: null
                        property bool canAppend: true

                        anchors.fill: parent

                        id: listViewDeals
                        delegate: offerComponent
                        clip: true
                        model: ListModel{}
                        onContentYChanged:
                        {
                            if(scrollDealsView.flickableItem.visibleArea.yPosition > 0.8){
                                if(nextUrl !== null)
                                    request(listViewDeals, nextUrl)
                            }
                        }

                        property alias isEmpty: listViewDeals_error.opa
                        ErrorComponent
                        {
                            id: listViewDeals_error
                            property string text: string.search_text
                        }
                    }
                    Component.onCompleted: {
                            dealsView.queryDeals = app.rootURL + "api/v1/deal"
                            request(listViewDeals, dealsView.queryDeals)
                            couponView.prevView = "deal"
                    }
                }

            }
        }

        //*************************************************//
        Rectangle {
            id: menuBar
            z: 5
            anchors.top: parent.top
            width: parent.width
            height: app.orientation == app.orientationLandscape ? 40*app.dp : menuHeight
            color: palette.colorPrimary
            Rectangle {
                id: menuButton
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                width: height
                height: parent.height
                scale: maMenuBar.pressed ? 1.2 : 1
                color: "transparent"
                MenuIconLive {
                    id: menuBackIcon
                    scale: (parent.height/height)*0.5
                    anchors.centerIn: parent
                    value: menuProgressOpening
                }
                MouseArea {
                    id: maMenuBar
                    anchors.fill: parent
                    onClicked:
                    {
                        if(menuView.x === 0)
                            menuView_back()
                        else
                          onMenu()
                    }
                }
                clip: true
            }
            Label {
                id: titleText
                anchors.left: menuButton.right
                anchors.verticalCenter: parent.verticalCenter
                anchors.leftMargin: - 10 * app.dp
                text: appTitle
                font.pixelSize: 0.25*parent.height
                color: palette.colorWhite
            }
            Label
            {
                id: lbOption
                anchors.fill: parent
                visible: true
                Image
                {
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.margins: parent.height * 0.2
                    anchors.rightMargin: app.dp * 132
                    fillMode: Image.PreserveAspectFit
                    source: "qrc:/images/ic_search.png"
                    scale: searchIconClick.pressed ? 1.2 : 1
                    MouseArea {
                        id: searchIconClick
                        anchors.fill: parent
                        onClicked: onDeals()
                    }
                }
                Image
                {
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    anchors.margins: parent.height * 0.2
                    anchors.rightMargin: app.dp * 94
                    fillMode: Image.PreserveAspectFit
                    source: "qrc:/images/ic_filter.png"
                    scale: filterIconClick.pressed ? 1.2 : 1
                    MouseArea {
                        id: filterIconClick
                        anchors.fill: parent
                        onClicked: {
                            if(filterView.height !== 0)
                                filter_back()
                            else
                                onFilter()
                        }
                    }
                }
                Rectangle{
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    width: app.dp * 80
                    anchors.topMargin: 15 * app.dp
                    anchors.bottomMargin: 15 * app.dp
                    anchors.rightMargin: 10 * app.dp
                    border.width: 2
                    border.color: palette.colorWhite
                    color: palette.colorPrimary
                    scale: maProvinces.pressed ? 1.2 : 1
                    Label{
                        anchors.fill: parent
                        text: provinceName
                        color: palette.colorWhite
                        verticalAlignment: Text.AlignVCenter
                        horizontalAlignment: Text.AlignHCenter
                    }
                    Image{
                        anchors.left: parent.left
                        anchors.leftMargin: 2 * app.dp
                        anchors.verticalCenter: parent.verticalCenter
                        height: app.dp * 15
                        fillMode: Image.PreserveAspectFit
                        source: "qrc:/images/ic_province.png"
                    }
                    MouseArea{
                        id: maProvinces
                        anchors.fill: parent
                        onClicked: {
                            if(provincesView.height !== 0)
                                provinces_back()
                            else
                                onProvinces()
                        }
                    }
                }

/*
                ComboBox {
                    id: listViewProvinces
                    anchors.right: parent.right
                    anchors.top: parent.top
                    anchors.bottom: parent.bottom
                    width: app.dp * 80
                    anchors.topMargin: 15 * app.dp
                    anchors.bottomMargin: 15 * app.dp
                    anchors.rightMargin: 10 * app.dp
                    model: ListModel {}
                    onCurrentIndexChanged:
                    {
                        provinceId = listViewProvinces.model.get(currentIndex).province_id
                        refreshDeal();
                    }

                    //onCurrentIndexChanged: //print(listViewProvinces.model.get(comboBox.currentIndex).province_name_s)

                    style: ComboBoxStyle {
                            id: comboBox
                            background: Rectangle {
                                id: rectCategory
                                radius: 5
                                border.width: 2
                                border.color: palette.colorWhite
                                color: palette.colorPrimary
                            }
                            label: Text {
                                verticalAlignment: Text.AlignVCenter
                                horizontalAlignment: Text.AlignHCenter
                                font.pointSize: 15
                                //font.family: "Courier"
                                //font.capitalization: Font.SmallCaps
                                color: palette.colorWhite
                                text: (provinceId === "all") ? "Tất cả" : listViewProvinces.model.get(control.currentIndex).province_name_s
                            }


                        }
                    Component.onCompleted: getProvinces(listViewProvinces)
                }*/
            }



        }
        Image {
            id: imgShadow
            anchors.top: menuBar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 6*app.dp
            z: 4
            source: "qrc:/images/shadow_title.png"
        }

        Loader {
            id: menuBottombar

            property int index: -1
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: menuHeight
            z:2
            source: "MenuBottombar.qml"
        }

        Image {
            id: imgShadowMenu
            anchors.top: menuBar.bottom
            anchors.bottom: brandsView.bottom
            anchors.left: brandsView.right
            width: 6*app.dp
            z: 5
            source: "qrc:/images/shadow_long.png"
            visible: brandsView.x != -app.width
        }
        //*************************************************//

        Rectangle
        {
            id: recCategories
            anchors.top: menuBar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: searchHeight
            color: palette.colorWhite

            property int currentId: 0
            ListView
            {
                id: listViewCategories
                anchors.fill: parent
                orientation: ListView.Horizontal
                delegate: Rectangle
                {
                    height: searchHeight
                    width: app.width / 4

                    property alias color: borderCategory.color
                    Rectangle
                    {
                        z: 5
                        id: borderCategory
                        anchors.bottom: parent.bottom
                        x: 0
                        width: app.width / 4
                        height: 5 * app.dp
                        color: id === 0 ? palette.colorPrimary : palette.colorWhite

                        Behavior on x { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }
                    }
                    Text
                    {
                        id: textCategory
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: category_name
                        font.pixelSize: parent.height * 0.3
                    }

                    MouseArea {
                        anchors.fill: parent
                        hoverEnabled: true
                        onPressed: {
                            if(app.menuIsShown)
                                menuView_back()
                        }
                        onClicked: {
                            if(menuIsShown)
                                menuView_back()
                            else
                            {
                                for(var i = 0; i <= listViewCategories.count; i++)
                                    listViewCategories.contentItem.children[i].color = palette.colorWhite

                                if(index === 0)
                                    listViewCategories.contentItem.children[index].color = palette.colorPrimary
                                else
                                    listViewCategories.contentItem.children[index + 1].color = palette.colorPrimary

                                if(categoryId !== id)
                                {
                                    categoryId = id
                                    refreshDeal()
                                }
                            }
                        }
                    }
                }
                clip: true
                model: ListModel {}
                Component.onCompleted: {
                    getCategories(listViewCategories)
                }

            }
        }


        Loader {
            id: loader
            anchors.top: menuBar.bottom
            anchors.topMargin: searchHeight
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: menuBottombar.top

            Behavior on anchors.topMargin { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

            property string queryLoader: null
            ScrollView {
                id: scrollLoader
             anchors.fill: parent
             flickableItem.interactive: true

             style: ScrollViewStyle {
                     transientScrollBars: true
                     handle: Item {
                         implicitWidth: 30
                         implicitHeight: 200
                         Rectangle {
                             color: palette.color_bg
                             anchors.fill: parent
                             anchors.topMargin: 6
                             anchors.leftMargin: 4
                             anchors.rightMargin: 4
                             anchors.bottomMargin: 6
                         }
                     }
                     scrollBarBackground: Item {
                         implicitWidth: 10
                         implicitHeight: 200
                     }
                 }

                ListView
                {
                    property string nextUrl: null
                    property bool canAppend: true

                    anchors.fill: parent
                    id: listViewLoader
                    delegate: offerComponent
                    clip: true
                    model: ListModel{}
                    onContentYChanged:
                    {
                        //print(scrollLoader.flickableItem.visibleArea.yPosition)
                        //if(scrollLoader.flickableItem.visibleArea.yPosition < - 0.05)
                           // refreshDeal()

                        if(scrollLoader.flickableItem.visibleArea.yPosition > 0.8){
                            if(nextUrl !== null)
                                request(listViewLoader, nextUrl)
                        }
                    }

                    property alias isEmpty: listViewLoader_error.opa
                    ErrorComponent
                    {
                        id: listViewLoader_error
                        property string text: string.empty_list
                    }
                }

                Component.onCompleted: {
                    refreshDeal()
                }
            }
        }
    }
    ReceivedDealComponent{
        id: receivedDealComponent
    }
    ReceivedDealHistoryComponent{
        id: receivedDealHistoryComponent
    }

    OfferComponent
    {
        id: offerComponent
    }
    NotiComponent
    {
        id: notiComponent
    }
    ProfileComponent
    {
        id: profileComponent
    }
    CouponComponent
    {
        id: couponComponent
    }
    BrandComponent
    {
        id: brandComponent
    }
    BrandsComponent
    {
        id: brandsComponent
    }

    Item
    {
        id: state_lib
        states:
        [
            State{
                name: "Load_Offer_Deal"
                PropertyChanges {
                    target: loader
                    anchors.topMargin: searchHeight
                }
                PropertyChanges {
                    target: menuBottombar
                    source: ""
                }
                PropertyChanges {
                    target: menuBottombar
                    source: "MenuBottombar.qml"
                }
                PropertyChanges {
                    target: listViewLoader_error
                    text: string.empty_list
                }
            },
            State{
                name: "Load_Favorite_Deal"
                PropertyChanges {
                    target: app
                    appTitle: string.tab_favorite                    
                    }
                PropertyChanges {
                    target: loader
                    anchors.topMargin: 0
                }
                PropertyChanges {
                    target: listViewLoader_error
                    text: string.empty_list_favorite
                }
            },
            State{
                name: "Load_Notification"
                PropertyChanges {
                    target: app
                    appTitle: string.tab_notification
                    }
                PropertyChanges {
                    target: lbOption
                    visible: false
                    }
                PropertyChanges {
                    target: listViewLoader_error
                    text: string.empty_list_message
                }
                PropertyChanges {
                    target: loader
                    anchors.topMargin: 0
                }
            },
            State{
                name: "Load_Profile"
                PropertyChanges {
                    target: app
                    appTitle: string.tab_profile
                    }
                PropertyChanges {
                    target: loader
                    anchors.topMargin: 0
                }
                PropertyChanges {
                    target: lbOption
                    visible: false
                    }
            },
            State{
                name: "Load_Deals_Search"
                PropertyChanges {
                    target: app
                    appTitle: string.app_name
                    }
                PropertyChanges {
                    target: listViewLoader_error
                    text: string.search_text
                }
            }
        ]
    }
    Rectangle
    {
        id: loginView
        z: -1
        anchors.fill: parent
        focus: true
        Keys.onBackPressed: {}
        property string prevView: "value"
        MouseArea
        {
            anchors.fill: parent
        }

        Image
        {
            anchors.fill: parent
            source: "qrc:/images/bg_tutorial.png"
            fillMode: Image.PreserveAspectCrop
        }
        Image
        {
            anchors.fill: parent
            source: "qrc:/images/img_tutorial_notify.png"
            fillMode: Image.PreserveAspectFit
        }
        Rectangle
        {
            id: recLogin
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: app.width / 2
            color: palette.colorWhite
        }
        Label
        {
            anchors.right: parent.right
            anchors.top: parent.top
            anchors.margins: 15 * app.dp
            text: "Bỏ qua"
            color: palette.colorWhite
            font.pixelSize: 15 * app.dp

            MouseArea
            {
                anchors.fill: parent
                onClicked:
                {                    
                    loginView_back()
                    //recLoginView_back()
                }
            }
        }
        Label
        {
            id: titleLogin
            anchors.top: recLogin.top
            anchors.topMargin: 20 * app.dp
            anchors.left: parent.left
            anchors.right: parent.right
            horizontalAlignment: Text.AlignHCenter

            text: "SĂN ƯU ĐÃI MỖI NGÀY!"
            font.pixelSize: 20 * app.dp
        }
        Rectangle
        {
            id: btnRegis
            anchors.top: btnLogin.bottom
            anchors.topMargin: 10 * app.dp
            anchors.horizontalCenter: parent.horizontalCenter
            height: 40 * app.dp
            width: 300 * app.dp
            border.color: palette.color_share_facebook
            radius: 4
            Text
            {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                anchors.rightMargin: 80 * app.dp
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                text: string.register
                color: palette.color_share_facebook
                font.pixelSize: 20 * app.dp
            }
            MouseArea
            {
                anchors.fill: parent
                onClicked:{
                    onRegister()
                }
            }
        }
        /*
        Rectangle
        {
            id: btnFbLogin
            anchors.top: btnLogin.bottom
            anchors.topMargin: 10 * app.dp
            anchors.horizontalCenter: parent.horizontalCenter
            height: 40 * app.dp
            width: 300 * app.dp
            border.color: palette.color_share_facebook
            radius: 4
            Text
            {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                anchors.rightMargin: 10 * app.dp
                verticalAlignment: Text.AlignVCenter
                text: string.fb_login_connect
                color: palette.color_share_facebook
                font.pixelSize: 20 * app.dp
            }
            Image
            {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.margins: 5 * app.dp

                source: "qrc:/images/ic_share_facebook.png"
                fillMode: Image.PreserveAspectFit
            }

            MouseArea
            {
                anchors.fill: parent
                onClicked:{
                    loginView_back()
                    userId = virtualUserId
                }
            }
        }
        */
        Rectangle
        {
            id: btnLogin
            anchors.top: titleLogin.bottom
            anchors.topMargin: 10 * app.dp
            anchors.horizontalCenter: parent.horizontalCenter
            height: 40 * app.dp
            width: 300 * app.dp
            border.color: palette.black_70_transparent
            radius: 4
            Text
            {
                anchors.top: parent.top
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                anchors.rightMargin: 30 * app.dp
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
                text: "Đăng nhập bằng tài khoản"
                color: palette.color_share_facebook
                font.pixelSize: 20 * app.dp
            }
            MouseArea
            {
                anchors.fill: parent
                onClicked: {
                    onRecLogin()
                    //loginView_back()
                    //userId = virtualUserId
                }
            }
        }
    }
    function isOver5m(date1, date2){        
        var diff = date2 - date1
        var m = Math.floor(diff / 1000)
        if(m > 60 * 5)
            return false
        return true
    }

    function isEmail(email)
    {
        for(var i = 0; i < email.length; i++)
        {
            if(email[i] === '@')
                return true
}
        return false
    }
    function isDOB(dob)
    {
        if(dob.length !== 10) return false
        if(dob[0] < 0 || dob[0] > 3) return false
        if(dob[1] < 0 || dob[1] > 9) return false
        if(dob[3] < 0 || dob[3] > 1) return false
        if(dob[4] < 0 || dob[4] > 9) return false
        if(dob[6] < 0 || dob[6] > 2) return false
        if(dob[7] < 0 || dob[7] > 9) return false
        if(dob[8] < 0 || dob[8] > 9) return false
        if(dob[9] < 0 || dob[9] > 9) return false
        if(dob[2] !== "-") return false
        if(dob[5] !== "-") return false

        return true
    }

    function post(url){
        if(url === "")
            return

        var xhr = new XMLHttpRequest();
        xhr.open("POST", url, true);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.setRequestHeader('Token', "1");
        xhr.setRequestHeader('user_id', app.userId);
        xhr.send();
    }
    function checkSuccess(url, callback) {
            var xhr = new XMLHttpRequest();           
            xhr.onreadystatechange = (function(myxhr) {
                return function() {
                    callback(myxhr);
                }
            })(xhr);
            xhr.open('GET', url, true);
            xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhr.setRequestHeader('Token', "1");
            xhr.setRequestHeader('user_id', app.userId);
            xhr.send('');
        }
    function getLogin(username, password){
        if(username === "" || password === "")
            return
        var url = "http://api.khuyenmai.re/api/v1/login?phone=" + username + "&password=" + password

        var xhr = new XMLHttpRequest();
        xhr.open("GET", url, true);
        xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
        xhr.setRequestHeader('Token', "1");

        xhr.onreadystatechange = function() {
            if (xhr.readyState === XMLHttpRequest.HEADERS_RECEIVED) {
            } else if(xhr.readyState === XMLHttpRequest.DONE) {
                var json = JSON.parse(xhr.responseText.toString())

                if(json.data !== null)
                {
                    userId = json.data.id
                    Storage.updateUserID(json.data.id)
                    onLoginSuccess()
                }
                else
                {
                    recLoginView.error = "*Sai mật khẩu"
                    recLoginView.password = ""
                }
            }
        }

        xhr.send();
    }


    function request(modelId, url) {
            if(url === "")
                return

            var xhr = new XMLHttpRequest();
            xhr.open("GET", url, true);
            xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhr.setRequestHeader('Token', "1");
            xhr.setRequestHeader('user_id', app.userId);

            xhr.onreadystatechange = function() {
                if (xhr.readyState === XMLHttpRequest.HEADERS_RECEIVED) {
                } else if(xhr.readyState === XMLHttpRequest.DONE) {
                    var json = JSON.parse(xhr.responseText.toString())

                    if(modelId.canAppend)
                            modelId.model.append(json.data)

                    if(modelId.model.count === 0)
                        modelId.isEmpty = 0.5
                    else
                        modelId.isEmpty = 0

                    if(json.next === null)
                    {
                        modelId.nextUrl = ""
                        modelId.canAppend = false
                    }
                    else
                    {
                        modelId.nextUrl = app.rootURL + json.next
                        modelId.canAppend = true
                    }
                }
            }

            xhr.send();
    }
    function requestItem(modelId, url) {
            var xhr = new XMLHttpRequest();
            xhr.open("GET", url, true);
            xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhr.setRequestHeader('Token', "1");
            xhr.setRequestHeader('user_id', app.userId);

            xhr.onreadystatechange = function() {
                if (xhr.readyState === XMLHttpRequest.HEADERS_RECEIVED) {
                } else if(xhr.readyState === XMLHttpRequest.DONE) {
                    var json = JSON.parse(xhr.responseText.toString())
                    modelId.model = json.data
                }
            }
        xhr.send();
    }
    function getCategories(modelId) {
            var url = app.rootURL + "/api/v1/init"

            var xhr = new XMLHttpRequest();
            xhr.open("GET", url, true);
            xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhr.setRequestHeader('Token', "1");

            modelId.model.append({"id": 0, "category_name":"Tất cả"})
            xhr.onreadystatechange = function() {
                if (xhr.readyState === XMLHttpRequest.HEADERS_RECEIVED) {
                } else if(xhr.readyState === XMLHttpRequest.DONE) {
                    var json = JSON.parse(xhr.responseText.toString())
                    modelId.model.append(json.data.categories)
                }
            }

            xhr.send();
    }
    function getProvinces(modelId) {
            var url = app.rootURL + "api/v1/province"

            var xhr = new XMLHttpRequest();
            xhr.open("GET", url, true);
            xhr.setRequestHeader('Content-type', 'application/x-www-form-urlencoded');
            xhr.setRequestHeader('Token', "1");

            modelId.model.append({"province_id": "all",
                                     "province_name":"Tất cả",
                                    "province_name_s":"Tất cả"})
            xhr.onreadystatechange = function() {
                if (xhr.readyState === XMLHttpRequest.HEADERS_RECEIVED) {
                } else if(xhr.readyState === XMLHttpRequest.DONE) {
                    var json = JSON.parse(xhr.responseText.toString())
                    modelId.model.append(json.data)
                }
            }

            xhr.send();
    }
    function onOfferField(){
        notiView_back()
        profileView_back()
        app.type = "deal"
        refreshDeal()
    }

    function onFavoriteField(){
        notiView_back()
        profileView_back()
        app.type = "favorite"
        refreshDeal()
    }

    function onNotiField(){
        profileView_back()
        notiView.z = 0
    }
    function onLoginSuccess(){
        onOfferField()
        recLoginView_back()
        loginView_back()
        //refreshDeal()
        refreshSearchDeal()
        refreshNoti()
        refreshBrands()
        refreshCoupon()
    }
    function onLogout(){
        Storage.updateUserID(-1)
        userId = Storage.getUserID()

        profileView_back()
        state_lib.state = 'Load_Offer_Deal';
        onOfferField()

        refreshDeal()
        refreshSearchDeal()
        refreshNoti()
        refreshBrands()
        refreshCoupon()
    }

    function onProfileField(){
        requestItem(listProfileView, "http://api.khuyenmai.re/api/v1/profile")
        notiView_back()
        profileView.z = 0
    }

    function onFilter()
    {
        provinces_back()
        filterView.height = app.dp * 70;
        filterView.visible = true
    }

    function onProvinces(){
        filter_back()
        provincesView.height = 90 * app.dp
        provincesView.visible = true
    }
    function onReceiveCoupon(){
        receiveCouponView.y = app.width / 2.5
        receiveCouponView.focus = true
    }
    function onReceiveCouponSuccess(){
        couponView.isReceived = true
        //receiveCouponView_back()
        receiveCouponSuccessView.y = app.width / 2.5
        receiveCouponSuccessView.focus = true
    }
    function onRecOKGift()
    {
        recOKGift.y = app.width / 2.5
    }

    function onMenu()
    {
        filter_back()
        provinces_back()
        menuView.x = 0
        menuView.focus = true

        brandsView_back()        
    }

    function onLogin(){
        loginView.z = 10
        loginView.focus = true;
    }

    function onBrand()
    {
        brandView.focus = true
        brandView.x = 0
    }

    function onBrands() {
        lbOption.visible = false

        brandsView.focus = true
        searchBrandsView.focus = true
        searchBrandsField.text = ""

        brandsView.x = 0
        searchBrandsView.x = 0

        menuView_back()        
    }
    function onAbout()
    {
        aboutView.x = 0
        aboutView.focus = true
        menuView_back()
    }

    function onCoupon()
    {
        couponView.focus = true
        couponView.x = 0
    }
    function onDeleteCoupon()
    {
        post(receivedDealView.deleteURL)
        recYN_back()
        refreshDealReceived()
        refreshCoupon()
    }

    function onDeals() {
        dealsView.focus = true
        searchDealsView.focus = true
        searchDealsField.text = ""

        dealsView.x = 0
        searchDealsView.x = 0
        filter_back()
        provinces_back()
    }
    function onLocation(){
        if(receiveCouponSuccessView.y === app.height)
        {
            locationView.focus = true
            locationView.x = 0
        }
    }
    function onReceivedDeal(){
        receivedDealView.x = 0
        receivedDealView.focus = true
    }
    function onGift(){
        giftView.x = 0
        giftView.focus = true
    }
    function onGiftDetail(){
        giftDetailView.x = 0
        giftDetailView.focus = true
    }

    function onReceivedDealHistory(){
        receivedDealHistoryView.x = 0
        receivedDealHistoryView.focus = true
    }
    function onInfoUser(){
        infoUserView.x = 0
        infoUserView.focus = true
    }

    function onRecLogin(){
        recLoginView.username = ""
        recLoginView.password = ""
        recLoginView.error = ""
        recLoginView.focus = true
        recLoginView.y = 0
    }

    function onRegister(){
        recRegisterView.focus = true
        recRegisterView.y = 0
    }
    function onResetPassword(){
        recResetPasswordView.focus = true
        recResetPasswordView.y = 0
    }    

    function onChangePassword(){
        recChangePasswordView.focus = true
        recChangePasswordView.y = 0
    }

    function receiveCouponView_back(){
        receiveCouponView.y = app.height
        couponView.focus = true
    }
    function receiveCouponSuccessView_back(){
        receiveCouponSuccessView.y = app.height
        couponView.focus = true
    }

    function provinces_back(){
        provincesView.height = 0
        provincesView.visible = false
    }

    function filter_back(){
        filterView.visible = false
        filterView.height = 0
    }

    function menuView_back(){
        menuView.x = - menuWidth
    }

    function recLoginView_back(){
        recLoginView.y = app.height
        loginView.focus = true
    }

    function recRegisterView_back(){
        recRegisterView.y = app.height
        loginView.focus = true
    }
    function recResetPasswordView_back(){
        recResetPasswordView.y = app.height
        loginView.focus = true
    }

    function recChangePasswordView_back(){
        recChangePasswordView.y = app.height
        infoUserView.focus = true
    }
    function loginView_back(){
        loginView.z = -1
        switch(loginView.prevView)
        {
        case "brands":
            brandsView.focus = true
            break
        case "brand":
            brandView.focus = true
            break
        case "coupon":
            couponView.focus = true
            break
        }
    }
    function receivedDealView_back(){
        if(recYN.y > app.height)
            receivedDealView.x = app.width
    }
    function giftView_back(){
        giftView.x = app.width
    }
    function giftDetailView_back(){
        if(recOKGift.y > app.height)
        {
            giftDetailView.x = app.width
            giftView.focus = true
        }
    }
    function receivedDealHistoryView_back(){
        receivedDealHistoryView.x = app.width
    }
    function infoUserView_back(){
        infoUserView.x = app.width
    }
    function recYN_back(){
        recYN.y = app.height + 100 * app.dp
    }
    function recOKGift_back()
    {
        recOKGift.y = app.height + 100 * app.dp
    }
    function aboutView_back()
    {
        aboutView.x = app.width
        onMenu()
    }

    function dealsView_back() {
        Qt.inputMethod.hide()
        normalView.focus = true
        dealsView.x =  -app.width
        searchDealsView.x = -app.width
    }
    function brandView_back() {
        brandsView.focus = true
        searchBrandsView.focus = true
        brandView.x =  app.width
    }
    function brandsView_back() {
        Qt.inputMethod.hide()
        if(brandsView.x === 0)
        {
            lbOption.visible = true
            searchBrandsView.x = app.width
            brandsView.x =  app.width
            onMenu()
            menuView.focus = true
        }
    }
    function couponView_back() {
        if(receiveCouponSuccessView.y === app.height)
        {
            couponView.x =  app.width

            if(couponView.prevView === "brand")
                brandView.focus = true
            else if(couponView.prevView === "deal")
                dealsView.focus = true
            else if(couponView.prevView === "received")
                receivedDealView.focus = true
            else if(couponView.prevView === "receivedHistory")
                receivedDealHistoryView.focus = true
        }
    }
    function locationView_back() {
        if(locationView.prevView === "brand")
            brandView.focus = true
        else if(locationView.prevView === "coupon")
            couponView.focus = true

        locationView.x =  app.width
    }
    function notiView_back(){
        notiView.z = -1
    }
    function profileView_back(){
        profileView.z = -1
    }

    function rightPassword(pass)
    {
        if(pass.length < 6)
            return false
        for(var i = 0; i <  pass.length; i++)
        {
            if(!((pass[i].charCodeAt() > 65 && pass[i].charCodeAt() < 91)
                    || (pass[i].charCodeAt() > 96 && pass[i].charCodeAt() < 123)
                    || (pass[i].charCodeAt() > 47 && pass[i].charCodeAt() < 58)))
                return false
        }
        return true
    }
    function rightPhone(phone)
    {
        if(phone.length < 9 || phone.length >14 || phone[0] !== "0")
            return false
        for(var i = 0; i < phone.length; i++)
        {
            if(!(phone[i].charCodeAt() > 47 && phone[i].charCodeAt() < 58))
                return false
        }
        return true
    }

    function prefixAddress(string)
    {
        if(string.length > 35)
            return string.substr(0, 34) + "..."

        return string
    }

    function getDay(time)
    {
        var tmp = time / (60 * 60 * 60)
        return parseInt(tmp)
    }

    function getHour(time)
    {
        var tmp = time - getDay(time) * 60 * 60 * 60
        var h = tmp / (60 * 60)
        return parseInt(h)
    }
    function getMinute(time)
    {
        var tmp = time - getDay(time) * 60 * 60 * 60 - getHour(time) * 60 * 60
        var m = tmp / 60
        return parseInt(m)
    }
    function getSecond(time)
    {
        var tmp = time - getDay(time) * 60 * 60 * 60 - getHour(time) * 60 * 60 - getMinute(time) * 60
        return parseInt(tmp)
    }

    function prefixLogo(string) // https -> http
    {
        while(string[string.length - 1] === " ")
        {
          string = string.substr(0, string.length - 1)
        }
        if(string[4] === 's')
            return string.substr(0, 4) + string.substr(5, string.length)
        return string
    }
    function prefixDate(string)
    {
        return string.substr(8, 2) + "-" + string.substr(5, 2) + "-" + string.substr(0, 4)
    }
    function fixDate(string)
    {
        return string.substr(6, 4) + "-" + string.substr(3, 2) + "-" + string.substr(0, 2)
    }
    function resetListView(modelId)
    {
        modelId.model.clear()
       // modelId.nextUrl = ""
        modelId.canAppend = true
        modelId.isEmpty = 0
    }

    function updateFilterBrands(string)
    {
        resetListView(listViewBrands)
        if(string === "")
            brandsView.queryBrands = app.rootURL + "api/v1/brand"
        else
            brandsView.queryBrands = app.rootURL + "api/v1/brand/?query=" + string
        request(listViewBrands, brandsView.queryBrands)
    }

    function updateFilterDeals(string)
    {
        resetListView(listViewDeals)
        if(string === "")
            dealsView.queryDeals = app.rootURl + "api/v1/deal"
        else
            dealsView.queryDeals = app.rootURL + "api/v1/deal/?query=" + string
        request(listViewDeals, dealsView.queryDeals)
    }

    function refreshDeal()
    {
        resetListView(listViewLoader)
        var _query = "http://api.khuyenmai.re/api/v1/"+ app.type + "/?province_id=" + provinceId + "&category_id=" + categoryId + "&sort=" + sortId

        request(listViewLoader, _query)
    }
    function refreshSearchDeal(){
        resetListView(listViewDeals)
        var _query = "http://api.khuyenmai.re/api/v1/"+ app.type + "/?province_id=" + provinceId + "&category_id=" + categoryId + "&sort=" + sortId

        request(listViewDeals, _query)
    }

    function refreshNoti(){
        resetListView(listViewLoader)
        var _query =  app.rootURL + "api/v1/message"
                //"http://api.khuyenmai.re/api/v1/" + app.type + "/?province_id=" + provinceId + "&category_id=" + categoryId + "&sort=" + sortId
        request(listViewLoader, _query)
    }
    function refreshBrands(){
        resetListView(listViewBrands)
        request(listViewBrands, brandsView.queryBrands)
    }
    function refreshCoupon(){
        requestItem(listViewCoupon, "http://api.khuyenmai.re/api/v1/deal/")
        requestItem(listViewCoupon, couponView.queryCoupon)
    }
    function refreshGiftDetail(){
        requestItem(listViewGiftDetail, "http://api.khuyenmai.re/api/v1/deal/")
        requestItem(listViewGiftDetail, giftDetailView.queryGiftDetail)
        //requestItem(listViewGiftDetail, couponView.queryCoupon)
    }
    function refreshDealReceived()
    {
        resetListView(listViewReceivedDeal)
        var _query = "http://api.khuyenmai.re/api/v1/deal/received"
        request(listViewReceivedDeal, _query)
    }

    // BrandView
    Rectangle{
        id: brandView
        width: parent.width
        height: parent.height
        x: app.width
        y: 0
        z: 5
        Behavior on x { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        property string queryBrand: null
        color: palette.colorWhite
        MouseArea
        {
            anchors.fill: parent
        }
        focus: true
        Keys.onBackPressed: brandView_back()
        Rectangle {
            id: brandView_menuBar
            z: 5
            anchors.top: parent.top
            anchors.topMargin: 0
            width: parent.width
            height: app.orientation == app.orientationLandscape ? 40*app.dp : menuHeight
            color: palette.colorWhite
            Rectangle {
                id: brandView_menuButton
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                width: 1.2*height
                height: parent.height
                scale: brandView_maMenuBar.pressed ? 1.2 : 1
                color: "transparent"
                Image {
                    anchors.centerIn: parent
                    height: parent.height * 0.5
                    fillMode: Image.PreserveAspectFit
                    source: "qrc:/images/ic_back_white_bg.png"
                }
                MouseArea {
                    id: brandView_maMenuBar
                    anchors.fill: parent
                    onClicked: brandView_back()
                }
                clip: true
            }
            Label {
                id: brandView_titleText
                anchors.right: parent.right
                anchors.rightMargin: 100 * app.dp
                anchors.verticalCenter: parent.verticalCenter
                text: string.brand
                font.pixelSize: 0.4*parent.height
                font.bold: true
                color: palette.colorPrimary
            }
        }
        Image {
            anchors.top: brandView_menuBar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 6*app.dp
            z: 5
            source: "qrc:/images/shadow_title.png"
        }
        Loader
        {
            anchors.top: brandView_menuBar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom

            ScrollView {
             anchors.fill: parent
             flickableItem.interactive: true

             style: ScrollViewStyle {
                     transientScrollBars: true
                     handle: Item {
                         implicitWidth: 30
                         implicitHeight: 200
                         Rectangle {
                             color: palette.colorPrimary
                             anchors.fill: parent
                             anchors.topMargin: 6
                             anchors.leftMargin: 4
                             anchors.rightMargin: 4
                             anchors.bottomMargin: 6
                         }
                     }
                     scrollBarBackground: Item {
                         implicitWidth: 10
                         implicitHeight: 200
                     }
                 }
                ListView
                {
                    property string nextUrl: null
                    property bool canAppend: true

                    anchors.fill: parent
                    id: listViewBrand
                    delegate: brandComponent
                    clip: true
                }
            }
        }
    }
    Rectangle{
        id: couponView
        property bool isReceived: false
        property string prevView: ""
        property string logoURL: null
        property string queryCoupon: null

        anchors.top: parent.top
        width: parent.width
        height: parent.height
        x: app.width
        z: 5
        Behavior on x { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        color: palette.colorWhite
        MouseArea
        {
            anchors.fill: parent
        }

        focus: true
        Keys.onBackPressed: couponView_back()

        Rectangle {
            id: couponView_menuBar
            width: parent.width
            height: app.orientation == app.orientationLandscape ? 40*app.dp : menuHeight
            color: palette.colorWhite
            Rectangle {
                id: couponView_menuButton
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                width: 1.2*height
                height: parent.height
                scale: couponView_maMenuBar.pressed ? 1.2 : 1
                color: "transparent"
                Image {
                    anchors.centerIn: parent
                    height: parent.height * 0.5
                    fillMode: Image.PreserveAspectFit
                    source: "qrc:/images/ic_back_white_bg.png"
                }
                MouseArea {
                    id: couponView_maMenuBar
                    anchors.fill: parent
                    onClicked: couponView_back()
                }
                clip: true

            }
            Image {
                anchors.right: parent.right
                anchors.top: parent.top
                anchors.margins: 0.2 * parent.height
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width / 2

                //height: app.dp * 30
                //width: parent.height * 1.5
                //anchors.centerIn: parent
                fillMode: Image.PreserveAspectFit
                source: couponView.logoURL
            }
        }
        Image {
            anchors.top: couponView_menuBar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 6*app.dp
            z: 4
            source: "qrc:/images/shadow_title.png"
        }
        Loader{

            focus : true
            Keys.onBackPressed: couponView_back()
            anchors.top: couponView_menuBar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom

            ScrollView {
               anchors.fill:parent
             flickableItem.interactive: true

             style: ScrollViewStyle {
                     transientScrollBars: true
                     handle: Item {
                         implicitWidth: 30
                         implicitHeight: 200
                         Rectangle {
                             color: palette.color_bg
                             anchors.fill: parent
                             anchors.topMargin: 6
                             anchors.leftMargin: 4
                             anchors.rightMargin: 4
                             anchors.bottomMargin: 6
                         }
                     }
                     scrollBarBackground: Item {
                         implicitWidth: 10
                         implicitHeight: 200
                     }
                 }
                ListView
                {
                    id: listViewCoupon
                    anchors.fill: parent
                    clip: true
                    delegate: couponComponent
                    model: ListModel{}
                }
            }
        }
    }
    Rectangle
    {       
        id: recLoginView
        //anchors.bottom: parent.bottom
        height: app.height
        width: parent.width
        color: palette.colorWhite
        y: app.height
        x: 0
        z: 10
        Behavior on y { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        property alias error: lbError.text
        property alias password: tfPassword.text
        property alias username: tfUsername.text
        MouseArea
        {
            anchors.fill: parent
        }

        focus: true
        Keys.onBackPressed: recLoginView_back()
        Text
        {
            id: lbSdt
            anchors.top: parent.top
            anchors.margins: 30 * app.dp
            anchors.horizontalCenter: parent.horizontalCenter
            text: "ĐĂNG NHẬP"
            font.pixelSize: 30 * app.dp
            color: palette.colorPrimary
            font.bold: true
        }

        TextField
        {
            id: tfUsername
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 100 * app.dp
            placeholderText: "Nhập SĐT"
            text: parent.username
            height: 50 * app.dp
            width: app.width * 0.8
            font.pixelSize: 15 * app.dp
            font.bold: true
            maximumLength: 14
            inputMethodHints: Qt.ImhFormattedNumbersOnly
            horizontalAlignment: TextInput.AlignHCenter
            style: TextFieldStyle {
                    textColor: palette.colorPrimary
                    background: Rectangle {
                        radius: 5
                        //implicitWidth: 100
                        //implicitHeight: 24
                        border.color: palette.color_bg
                        border.width: 5
                    }
                }
        }
        TextField
        {
            id: tfPassword
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 170 * app.dp
            placeholderText: "Nhập mật khẩu"
            text: parent.password
            font.bold: true
            height: 50 * app.dp
            width: app.width * 0.8
            font.pixelSize: 15 * app.dp
            maximumLength: 14
            echoMode: TextInput.Password
            horizontalAlignment: TextInput.AlignHCenter
            style: TextFieldStyle {
                    textColor: palette.colorPrimary
                    background: Rectangle {
                        radius: 5
                        border.color: palette.color_bg
                        border.width: 5
                    }
                }
        }
        Label{
            id: lbError
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: tfPassword.bottom
            anchors.margins: 5 * app.dp
            width: app.width * 0.7
            text: parent.error
            font.pixelSize: 10 * app.dp
            color: palette.colorPrimary
        }

        ListView{
            id: listViewLogin
            model: ListModel{}
        }

        Label{
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: tfPassword.bottom
            anchors.topMargin: 20 * app.dp
            width: app.width * 0.9
            Button
            {
                anchors.right: parent.right
                MouseArea{
                    anchors.fill: parent

                    onClicked:{
                        if(recLoginView.username === "" && recLoginView.password === "")
                        {
                            recLoginView.error = "*Cần điền SĐT và Mật khẩu"
                            recLoginView.username = ""
                            recLoginView.password = ""
                        }
                        getLogin(recLoginView.username, recLoginView.password,userId)
                    }
                }
                Text
                {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: app.width / 30
                    text: "Đăng nhập"
                    color: palette.colorWhite
                }


                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: app.width * 0.25
                            implicitHeight: implicitWidth / 2
                            color: palette.color_follow
                            //border.width: control.activeFocus ? 2 : 1
                            radius: 80
                        }
                    }
            }
            Button
            {
                anchors.horizontalCenter: parent.horizontalCenter
                MouseArea{
                    anchors.fill: parent

                    onClicked: onResetPassword()
                }
                Text
                {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: app.width / 30
                    text: "Quên mật khẩu"
                    color: palette.colorWhite
                }


                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: app.width * 0.25
                            implicitHeight: implicitWidth / 2
                            color: palette.color_follow
                            //border.width: control.activeFocus ? 2 : 1
                            radius: 80
                        }
                    }
            }
            Button
            {
                anchors.left: parent.left
                MouseArea{
                    anchors.fill: parent
                    onClicked: recLoginView_back()
                }
                Text
                {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: app.width / 30
                    text: "Hủy"
                    color: palette.colorWhite
                }


                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: app.width * 0.25
                            implicitHeight: implicitWidth / 2
                            color: palette.color_follow
                            //border.width: control.activeFocus ? 2 : 1
                            radius: 80
                        }
                    }
            }
        }
    }
    Rectangle
    {
        id: recRegisterView
        //anchors.bottom: parent.bottom
        height: app.height
        width: parent.width
        color: palette.colorWhite
        y: app.height
        x: 0
        z: 10
        Behavior on y { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        property alias error: lbError.text
        property alias password: tfPassword.text
        property alias username: tfUsername.text
        MouseArea
        {
            anchors.fill: parent
        }

        focus: true
        Keys.onBackPressed: recRegisterView_back()
        Text
        {
            anchors.top: parent.top
            anchors.margins: 30 * app.dp
            anchors.horizontalCenter: parent.horizontalCenter
            text: "ĐĂNG KÍ"
            font.pixelSize: 30 * app.dp
            color: palette.colorPrimary
            font.bold: true
        }

        TextField
        {
            id: tfResgisUsername
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 100 * app.dp
            placeholderText: "Nhập SĐT"
            text: ""
            readOnly: tfResgisOTP.visible
            height: 50 * app.dp
            width: app.width * 0.8
            font.pixelSize: 15 * app.dp
            font.bold: true
            maximumLength: 14
            inputMethodHints: Qt.ImhFormattedNumbersOnly
            horizontalAlignment: TextInput.AlignHCenter
            style: TextFieldStyle {
                    textColor: palette.colorPrimary
                    background: Rectangle {
                        radius: 5
                        //implicitWidth: 100
                        //implicitHeight: 24
                        border.color: tfResgisUsername.readOnly ? palette.color_bg : palette.colorPrimary
                        border.width: 5
                    }
                }
        }
        TextField
        {
            id: tfResgisOTP
            //anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: tfResgisUsername.bottom
            anchors.left: tfResgisUsername.left
            anchors.topMargin: (height === 0) ?  0 : 20 * app.dp
            placeholderText: "Nhập mã OTP"

            readOnly: tfResgisPassword1.visible
            visible: false
            height: visible ? 50 * app.dp : 0
            Behavior on height { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

            text: ""
            width: app.width * 0.6
            font.pixelSize: 15 * app.dp
            font.bold: true
            maximumLength: 14
            inputMethodHints: Qt.ImhFormattedNumbersOnly
            horizontalAlignment: TextInput.AlignHCenter
            style: TextFieldStyle {
                    textColor: palette.colorPrimary
                    background: Rectangle {
                        radius: 5
                        //implicitWidth: 100
                        //implicitHeight: 24
                        border.color: tfResgisOTP.readOnly ? palette.color_bg : palette.colorPrimary
                        border.width: 5
                    }
                }

        }
        Image
        {
            anchors.top: tfResgisUsername.bottom
            anchors.topMargin: 30 * app.dp
            anchors.right: tfResgisUsername.right
            anchors.rightMargin: 20 * app.dp
            height: 30 * app.dp
            scale: maRegis.pressed ? 1.2 : 1
            fillMode: Image.PreserveAspectFit
            source: "qrc:/images/refresh_ic.png"
            smooth: true
            antialiasing: true
            visible: tfResgisOTP.visible
            MouseArea
            {
                id: maRegis
                anchors.fill: parent
                onClicked:
                {
                    otp = Math.ceil(Math.random() * 100000)
                     Storage.updateOTP(otp)
                    print(otp)
                    tfResgisOTP.text = ""
                    var url = "http://api.khuyenmai.re/api/v1/sendSMS?phone="+tfResetPassUsername.text +"&code=" + otp
                    post(url)
                }
            }
        }


        TextField
        {
            id: tfResgisPassword1
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: tfResgisOTP.bottom
            anchors.topMargin: (height === 0) ?  0 : 20 * app.dp
            placeholderText: "Nhập mật khẩu"

            visible: false
            height: visible ? 50 * app.dp : 0
            Behavior on height { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

            text: ""
            width: app.width * 0.8
            font.pixelSize: 15 * app.dp
            font.bold: true
            maximumLength: 14
            echoMode: TextInput.Password
            horizontalAlignment: TextInput.AlignHCenter
            style: TextFieldStyle {
                    textColor: palette.colorPrimary
                    background: Rectangle {
                        radius: 5
                        //implicitWidth: 100
                        //implicitHeight: 24
                        border.color: tfResgisPassword1.readOnly ? palette.color_bg : palette.colorPrimary
                        border.width: 5
                    }
                }
        }
        TextField
        {
            id: tfResgisPassword2
            anchors.horizontalCenter: parent.horizontalCenter
             anchors.top: tfResgisPassword1.bottom
            anchors.topMargin: (height === 0) ?  0 : 20 * app.dp
            placeholderText: "Nhập lại mật khẩu"

            visible: false
            height: visible ? 50 * app.dp : 0
            Behavior on height { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }


            text: ""
            width: app.width * 0.8
            font.pixelSize: 15 * app.dp
            font.bold: true
            maximumLength: 14
            echoMode: TextInput.Password
            horizontalAlignment: TextInput.AlignHCenter
            style: TextFieldStyle {
                    textColor: palette.colorPrimary
                    background: Rectangle {
                        radius: 5
                        //implicitWidth: 100
                        //implicitHeight: 24
                        border.color: tfResgisPassword1.readOnly ? palette.color_bg : palette.colorPrimary
                        border.width: 5
                    }
                }
        }


        Label{
            id: lbRegisterError
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: tfResgisPassword2.bottom
            anchors.margins: 5 * app.dp
            width: app.width * 0.7
            text: parent.error
            font.pixelSize: 10 * app.dp
            color: palette.colorPrimary
        }


        Label{
            anchors.right: parent.right
            anchors.top: tfResgisPassword2.bottom
            anchors.margins: 20 * app.dp
            width: app.width * 0.7

            Button
            {
                anchors.right: parent.right
                MouseArea{
                    property int _otp: 0
                    anchors.fill: parent

                    onClicked:{
                        if(!tfResgisOTP.visible)
                        {
                            var url = "http://api.khuyenmai.re/api/v1/profile/" + tfResgisUsername.text
                            var tmp = 0
                            checkSuccess(url, function (o) {
                                                    var d = o.responseText;
                                if(d !== "")
                                {
                                    if(d === "1")
                                        lbRegisterError.text = "*SĐT đã tồn tại"
                                    else if(!rightPhone(tfResgisUsername.text))
                                       lbRegisterError.text = "*SĐT không hợp lệ"
                                    else
                                    {
                                       lbRegisterError.text = ""
                                       tfResgisOTP.visible = true
                                       tfResgisOTP.text = ""
                                       otp = Math.ceil(Math.random() * 100000)
                                        otpDate = new Date()
                                        Storage.updateOTP(otp)
                                        Storage.updateOTPDate(otpDate)
                                       //lbRegisterError.text = otp

                                        tmp++
                                        if(tmp === 1)
                                        {
                                            print(otp)
                                            var url = "http://api.khuyenmai.re/api/v1/sendSMS?phone="+tfResgisUsername.text +"&code=" + otp

                                            post(url)
                                        }
                                    }
                                }                                
                            });
                        }
                        else if(!tfResgisPassword1.visible)
                        {
                            if(tfResgisOTP.text === Storage.getOTP())
                            {
                                if(isOver5m(otpDate, new Date()))
                                {
                                    tfResgisPassword1.visible = true
                                    tfResgisPassword1.text = ""
                                    tfResgisPassword2.visible = true
                                    tfResgisPassword2.text = ""
                                }
                                else
                                    lbRegisterError.text = "Mã OTP quá hạn"
                            }
                            else
                            {
                                lbRegisterError.text = "Sai mã OTP"
                            }
                        }
                        else
                        {
                            if(tfResgisPassword1.text !== tfResgisPassword2.text)
                                lbRegisterError.text = "*Mật khẩu không trùng nhau"
                            else if(!rightPassword(tfResgisPassword1.text))
                                lbRegisterError.text = "*Mật khẩu lớn hơn 6 kí tự, bao gồm A-Z a-z 0-9"
                            else
                            {
                                url = "http://api.khuyenmai.re/api/v1/register?phone=" + tfResgisUsername.text + "&password=" + tfResgisPassword1.text
                                post(url)
                                recRegisterView_back()
                                onRecLogin()
                                tfResgisPassword1.visible = false
                                tfResgisPassword1.text = ""
                                tfResgisPassword2.visible = false
                                tfResgisPassword2.text = ""
                                tfResgisOTP.visible = false
                                tfResgisOTP.text = ""
                                tfResgisUsername.text = ""
                            }
                        }

                    }
                }
                Text
                {
                    id: btnRegisterNext
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: app.width / 30
                    text: tfResgisPassword1.visible ? "Đăng kí" : "Tiếp theo"
                    color: palette.colorWhite
                }


                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: app.width * 0.25
                            implicitHeight: implicitWidth / 2
                            color: palette.color_follow
                            //border.width: control.activeFocus ? 2 : 1
                            radius: 80
                        }
                    }
            }
            Button
            {
                anchors.right: parent.right
                anchors.rightMargin: 110 * app.dp
                MouseArea{
                    anchors.fill: parent
                    onClicked: recRegisterView_back()
                }
                Text
                {
                    id: btnRegisterCancel
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: app.width / 30
                    text: "Hủy"
                    color: palette.colorWhite
                }


                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: app.width * 0.25
                            implicitHeight: implicitWidth / 2
                            color: palette.color_follow
                            //border.width: control.activeFocus ? 2 : 1
                            radius: 80
                        }
                    }
            }
            Button
            {
                visible: tfResgisOTP.visible
                anchors.right: parent.right
                anchors.rightMargin: 220 * app.dp
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        lbRegisterError.text = ""
                        if(tfResgisPassword1.visible)
                        {
                            tfResgisPassword1.visible = false
                            tfResgisPassword2.visible = false
                        }
                        else if(tfResgisOTP.visible)
                        {
                            tfResgisOTP.visible = false
                        }
                    }
                }
                Text
                {
                    id: btnRegisterPrev
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: app.width / 30
                    text: "Quay lại"
                    color: palette.colorWhite
                }


                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: app.width * 0.25
                            implicitHeight: implicitWidth / 2
                            color: palette.color_follow
                            //border.width: control.activeFocus ? 2 : 1
                            radius: 80
                        }
                    }
            }
        }

    }

    Rectangle
    {
        id: recResetPasswordView
        //anchors.bottom: parent.bottom
        height: app.height
        width: parent.width
        color: palette.colorWhite
        y: app.height
        x: 0
        z: 10
        Behavior on y { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        //property alias error: lbError.text
        //property alias password: tfPassword.text
        //property alias username: tfUsername.text
        MouseArea
        {
            anchors.fill: parent
        }

        focus: true
        Keys.onBackPressed: recResetPasswordView_back()
        Text
        {
            anchors.top: parent.top
            anchors.margins: 30 * app.dp
            anchors.horizontalCenter: parent.horizontalCenter
            text: "KHÔI PHỤC TÀI KHOẢN"
            font.pixelSize: 30 * app.dp
            color: palette.colorPrimary
            font.bold: true
        }

        TextField
        {
            id: tfResetPassUsername
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 100 * app.dp
            placeholderText: "Nhập SĐT"
            text: ""
            readOnly: tfResetPassOTP.visible
            height: 50 * app.dp
            width: app.width * 0.8
            font.pixelSize: 15 * app.dp
            font.bold: true
            maximumLength: 14
            inputMethodHints: Qt.ImhFormattedNumbersOnly
            horizontalAlignment: TextInput.AlignHCenter
            style: TextFieldStyle {
                    textColor: palette.colorPrimary
                    background: Rectangle {
                        radius: 5
                        //implicitWidth: 100
                        //implicitHeight: 24
                        border.color: tfResetPassUsername.readOnly ? palette.color_bg : palette.colorPrimary
                        border.width: 5
                    }
                }
        }
        TextField
        {
            id: tfResetPassOTP
            //anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: tfResetPassUsername.bottom
            anchors.left: tfResetPassUsername.left
            anchors.topMargin: (height === 0) ?  0 : 20 * app.dp
            placeholderText: "Nhập mã OTP"

            readOnly: tfResetPass1.visible
            visible: false
            height: visible ? 50 * app.dp : 0
            Behavior on height { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

            text: ""
            width: app.width * 0.6
            font.pixelSize: 15 * app.dp
            font.bold: true
            maximumLength: 14
            inputMethodHints: Qt.ImhFormattedNumbersOnly
            horizontalAlignment: TextInput.AlignHCenter
            style: TextFieldStyle {
                    textColor: palette.colorPrimary
                    background: Rectangle {
                        radius: 5
                        //implicitWidth: 100
                        //implicitHeight: 24
                        border.color: tfResetPassOTP.readOnly ? palette.color_bg : palette.colorPrimary
                        border.width: 5
                    }
                }
        }
        Image
        {
            anchors.top: tfResetPassUsername.bottom
            anchors.topMargin: 30 * app.dp
            anchors.right: tfResetPassUsername.right
            anchors.rightMargin: 20 * app.dp
            height: 30 * app.dp
            scale: maReset.pressed ? 1.2 : 1
            fillMode: Image.PreserveAspectFit
            source: "qrc:/images/refresh_ic.png"
            smooth: true
            antialiasing: true
            visible: tfResetPassOTP.visible
            MouseArea
            {
                id: maReset
                anchors.fill: parent
                onClicked:
                {
                    otp = Math.ceil(Math.random() * 100000)
                     Storage.updateOTP(otp)
                    tfResetPassOTP.text = ""
                    print(otp)
                    var url = "http://api.khuyenmai.re/api/v1/sendSMS?phone="+tfResetPassUsername.text +"&code=" + otp
                    post(url)
                }
            }
        }


        TextField
        {
            id: tfResetPass1
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: tfResetPassOTP.bottom
            anchors.topMargin: (height === 0) ?  0 : 20 * app.dp
            placeholderText: "Nhập mật khẩu"

            visible: false
            height: visible ? 50 * app.dp : 0
            Behavior on height { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

            text: ""
            width: app.width * 0.8
            font.pixelSize: 15 * app.dp
            font.bold: true
            maximumLength: 14
            echoMode: TextInput.Password
            horizontalAlignment: TextInput.AlignHCenter
            style: TextFieldStyle {
                    textColor: palette.colorPrimary
                    background: Rectangle {
                        radius: 5
                        //implicitWidth: 100
                        //implicitHeight: 24
                        border.color: tfResetPass1.readOnly ? palette.color_bg : palette.colorPrimary
                        border.width: 5
                    }
                }
        }
        TextField
        {
            id: tfResetPass2
            anchors.horizontalCenter: parent.horizontalCenter
             anchors.top: tfResetPass1.bottom
            anchors.topMargin: (height === 0) ?  0 : 20 * app.dp
            placeholderText: "Nhập lại mật khẩu"

            visible: false
            height: visible ? 50 * app.dp : 0
            Behavior on height { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }


            text: ""
            width: app.width * 0.8
            font.pixelSize: 15 * app.dp
            font.bold: true
            maximumLength: 14
            echoMode: TextInput.Password
            horizontalAlignment: TextInput.AlignHCenter
            style: TextFieldStyle {
                    textColor: palette.colorPrimary
                    background: Rectangle {
                        radius: 5
                        //implicitWidth: 100
                        //implicitHeight: 24
                        border.color: tfResetPass1.readOnly ? palette.color_bg : palette.colorPrimary
                        border.width: 5
                    }
                }
        }


        Label{
            id: lbResetPassError
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: tfResetPass2.bottom
            anchors.margins: 5 * app.dp
            width: app.width * 0.7
            text: parent.error
            font.pixelSize: 10 * app.dp
            color: palette.colorPrimary
        }


        Label{
            anchors.right: parent.right
            anchors.top: tfResetPass2.bottom
            anchors.margins: 20 * app.dp
            width: app.width * 0.7

            Button
            {
                anchors.right: parent.right
                MouseArea{
                    anchors.fill: parent

                    onClicked:{
                        if(!tfResetPassOTP.visible)
                        {
                            var url = "http://api.khuyenmai.re/api/v1/profile/" + tfResetPassUsername.text
                            var tmp = 0
                            checkSuccess(url, function (o) {
                                                    var d = o.responseText;
                                if(d !== "")
                                {
                                    if(d === "1")
                                    {
                                        lbResetPassError.text = ""
                                        tfResetPassOTP.visible = true
                                        tfResetPassOTP.text = ""
                                        otp = Math.ceil(Math.random() * 100000)
                                         Storage.updateOTP(otp)
                                        //lbResetPassError.text = otp

                                        tmp++
                                        if(tmp === 1)
                                        {
                                            print(otp)
                                            var url = "http://api.khuyenmai.re/api/v1/sendSMS?phone="+tfResetPassUsername.text +"&code=" + otp
                                            post(url)
                                        }
                                    }
                                    else
                                    {
                                        if(!rightPhone(tfResgisUsername.text))
                                            lbResetPassError.text = "*SĐT không hợp lệ"
                                    }
                                }
                            });
                        }


                        else if(!tfResetPass1.visible)
                        {
                            if(tfResetPassOTP.text === Storage.getOTP())
                            {
                                if(isOver5m(otpDate, new Date()))
                                {
                                    tfResetPass1.visible = true
                                    tfResetPass1.text = ""
                                    tfResetPass2.visible = true
                                    tfResetPass2.text = ""
                                }
                                else
                                    lbRegisterError.text = "Mã OTP quá hạn"
                            }
                            else
                                lbResetPassError.text = "*Sai mã OTP"
                        }
                        else
                        {
                            if(tfResetPass1.text !== tfResetPass2.text)
                                lbResetPassError.text = "*Mật khẩu không trùng nhau"
                            else if(!rightPassword(tfResetPass1.text))
                                lbResetPassError.text = "*Mật khẩu lớn hơn 6 kí tự, bao gồm A-Z a-z 0-9"
                            else
                            {
                                url = "http://api.khuyenmai.re/api/v1/reset?phone=" + tfResetPassUsername.text + "&password=" + tfResetPass1.text
                                post(url)
                                print(url)
                                recResetPasswordView_back()
                                onRecLogin()
                                tfResetPass1.visible = false
                                tfResetPass1.text = ""
                                tfResetPass2.visible = false
                                tfResetPass2.text = ""
                                tfResetPassOTP.visible = false
                                tfResetPassOTP.text = ""
                                tfResetPassUsername.text = ""
                                lbResetPassError.text = ""
                            }
                        }

                    }
                }
                Text
                {
                    id: btnResetPassNext
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: app.width / 30
                    text:  tfResetPass1.visible ? "Khôi phục" : "Tiếp theo"
                    color: palette.colorWhite
                }


                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: app.width * 0.25
                            implicitHeight: implicitWidth / 2
                            color: palette.color_follow
                            //border.width: control.activeFocus ? 2 : 1
                            radius: 80
                        }
                    }
            }
            Button
            {
                anchors.right: parent.right
                anchors.rightMargin: 110 * app.dp
                MouseArea{
                    anchors.fill: parent
                    onClicked: recResetPasswordView_back()
                }
                Text
                {
                    id: btnResetPassCancel
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: app.width / 30
                    text: "Hủy"
                    color: palette.colorWhite
                }


                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: app.width * 0.25
                            implicitHeight: implicitWidth
                            color: palette.color_follow
                            //border.width: control.activeFocus ? 2 : 1
                            radius: 80
                        }
                    }
            }
            Button
            {
                visible: tfResetPassOTP.visible
                anchors.right: parent.right
                anchors.rightMargin: 220 * app.dp
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        lbResetPassError.text = ""
                        if(tfResetPass1.visible)
                        {
                            tfResetPass1.visible = false
                            tfResetPass2.visible = false
                        }
                        else if(tfResetPassOTP.visible)
                        {
                            tfResetPassOTP.visible = false
                        }
                    }
                }
                Text
                {
                    id: btnResetPassPrev
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: app.width / 30
                    text: "Quay lại"
                    color: palette.colorWhite
                }


                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: app.width * 0.25
                            implicitHeight: implicitWidth
                            color: palette.color_follow
                            //border.width: control.activeFocus ? 2 : 1
                            radius: 80
                        }
                    }
            }
        }

    }

    Rectangle{
        id: locationView
        property string prevView: null
        anchors.top: parent.top
        width: parent.width
        height: parent.height
        x: app.width
        z: 5
        Behavior on x { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        property string logoURL: null
        color: palette.colorWhite
        MouseArea
        {
            anchors.fill: parent
        }

        focus: true
        Keys.onBackPressed: locationView_back()
        Rectangle {
            id: locationView_menuBar
            anchors.top: parent.top
            anchors.topMargin: 0
            width: parent.width
            height: app.orientation == app.orientationLandscape ? 40*app.dp : menuHeight
            color: palette.colorPrimary
            Rectangle {
                id: locationView_menuButton
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                width: 1.2*height
                height: parent.height
                scale: locationView_maMenuBar.pressed ? 1.2 : 1
                color: "transparent"
                Image {
                    anchors.centerIn: parent
                    height: parent.height * 0.5
                    fillMode: Image.PreserveAspectFit
                    source: "qrc:/images/ic_back.png"
                }
                MouseArea {
                    id: locationView_maMenuBar
                    anchors.fill: parent
                    onClicked: locationView_back()
                }
                clip: true

            }
        }
        Image {
            anchors.top: locationView_menuBar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 6*app.dp
            z: 4
            source: "qrc:/images/shadow_title.png"
        }

        Label{
            id: lbAddress
            anchors.top: locationView_menuBar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: text.bottom
            wrapMode: Text.WordWrap
            text: app.address
        }

        Plugin {
                id: osmPlugin
                name: "osm"
                PluginParameter { name: "osm.useragent"; value: "My great Qt OSM application" }
                    PluginParameter { name: "osm.mapping.host"; value: "http://osm.tile.server.address/" }
                    PluginParameter { name: "osm.mapping.copyright"; value: "All mine" }
                    PluginParameter { name: "osm.routing.host"; value: "http://osrm.server.address/viaroute" }
                    PluginParameter { name: "osm.geocoding.host"; value: "http://geocoding.server.address" }
            }


            Map {
                id: map
                anchors.top: lbAddress.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.bottom: parent.bottom

                plugin: Plugin {name: "osm"}
                zoomLevel: 17
                center {
                    latitude: app.latitude
                    longitude: app.longitude
                }

                MapQuickItem {
                    id:marker
                    sourceItem: Image{
                        id: imageMap
                        source: "qrc:/images/ic_near_by_red.png"
                    }
                    coordinate: QtPositioning.coordinate( app.latitude, app.longitude)
                }
            }
    }
    Rectangle {
        id: menuView
        anchors.bottom: parent.bottom
        height: app.height - menuBar.height
        width: menuWidth
        z: 10
        focus: true
        Keys.onBackPressed: menuView_back()

        x: -menuWidth
        Behavior on x { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }
        onXChanged: {
            menuProgressOpening = (1-Math.abs(menuView.x/menuWidth))
        }
        MouseArea {
            anchors.right: parent.right
            anchors.rightMargin: app.menuIsShown ? (menuWidth - app.width) : -widthOfSeizure
            anchors.top: parent.top
            width: app.menuIsShown ? (menuWidth - app.width) : widthOfSeizure
            height: parent.height
            /*
            drag {
                target: menuView
                axis: Drag.XAxis
                minimumX: -menuView.width
                maximumX: 0
            }
            */
            /*
            onClicked: {
                if(app.menuIsShown) app.onMenu()
            }*/
            onReleased: {
                if( Math.abs(menuView.x) > 0.5*app.width ) {
                    menuView.x = - menuWidth //close the menu
                } else {
                   menuView.x = 0 //fully opened
                }
            }
        }
        MainMenu {
            id: mainMenu
            anchors.fill: parent
            onMenuItemClicked: {
                switch(page)
                {
                case "brands":
                    onBrands()
                    break;
                case "about":
                    onAbout()
                    break;
                }
            }
        }
    }

    Rectangle
    {
        id: filterView
        width: 100 * app.dp
        x: app.dp * 200
        y: menuHeight
        height: 0
        visible: false
        Behavior on height { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }
        Behavior on visible { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        MouseArea{
            anchors.fill: parent
        }
        Rectangle{
            id: filterSortA
            width: parent.width
            height: parent.height / 2
            color: maNew.pressed ? palette.color_bg : palette.colorPrimary
            Label {
                text: "Mới nhất"
                color: maNew.pressed ? palette.colorPrimary : palette.color_bg
                anchors.fill: parent
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
            Image{
                anchors.right: parent.right
                anchors.margins: 5 * app.dp
                anchors.verticalCenter: parent.verticalCenter
                height: app.dp * 10
                fillMode: Image.PreserveAspectFit
                source: "qrc:/images/ic_check.png"
                visible: (app.sortId === "new") ? true : false
            }
            MouseArea
            {
                id: maNew
                anchors.fill: parent
                onClicked: {
                    app.sortId = "new"
                    filter_back()
                    refreshDeal()                    
                }
            }
        }
        Rectangle
        {
            anchors.bottom: filterSortA.bottom
            height: 1
            color: palette.color_bg
            width: parent.width
            z: 3
        }

        Rectangle{
            id: filterSortD
            anchors.top: filterSortA.bottom
            width: parent.width
            height: parent.height / 2
            color: maOld.pressed ? palette.color_bg : palette.colorPrimary

            Label {
                text: "Cũ nhất"
                color: maOld.pressed ? palette.colorPrimary : palette.color_bg
                anchors.fill: parent
                verticalAlignment: Text.AlignVCenter
                horizontalAlignment: Text.AlignHCenter
            }
            Image{
                anchors.right: parent.right
                anchors.margins: 5 * app.dp
                anchors.verticalCenter: parent.verticalCenter
                height: app.dp * 10
                fillMode: Image.PreserveAspectFit
                source: "qrc:/images/ic_check.png"
                visible: (app.sortId === "old") ? true : false
            }
            MouseArea
            {
                id: maOld
                anchors.fill: parent
                onClicked: {
                    app.sortId = "old"
                    filter_back()
                    refreshDeal()
                }
            }
        }
    }
    Rectangle{
        id: provincesView
        width: 100 * app.dp
        height: 0
        anchors.right: parent.right
        anchors.rightMargin: 5
        y: menuHeight
        visible: false

        Behavior on height { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }
        Behavior on visible { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        MouseArea{
            anchors.fill: parent
        }
        color: palette.colorPrimary

        ListView{
            id: listViewProvinces
            anchors.fill: parent
            model: ListModel{}
            clip: true
            delegate: Rectangle{
                height: app.dp * 30
                width: parent.width
                color: maProvince.pressed ? palette.color_bg : palette.colorPrimary

                Label{
                    anchors.fill: parent
                    text: province_name_s
                    color: maProvince.pressed ? palette.colorPrimary : palette.color_bg
                    verticalAlignment: Text.AlignVCenter
                    horizontalAlignment: Text.AlignHCenter
                }
                Rectangle{
                    anchors.bottom: parent.bottom
                    height: 10
                    color: palette.color_bg
                }
                Image{
                    anchors.right: parent.right
                    anchors.margins: 5 * app.dp
                    anchors.verticalCenter: parent.verticalCenter
                    height: app.dp * 10
                    fillMode: Image.PreserveAspectFit
                    source: "qrc:/images/ic_check.png"
                    visible: (app.provinceId === province_id) ? true : false
                }

                MouseArea{
                    id: maProvince
                    anchors.fill: parent
                    onClicked:{
                        if(provinceId !== province_id)
                        {
                            Storage.updateProvinceID(province_id)
                            provinceId = Storage.getProvinceID()
                            Storage.updateProvinceName(province_name_s)
                            provinceName = Storage.getProvinceName()
                            provinces_back()
                            refreshDeal()
                        }
                    }
                }
            }
            Component.onCompleted: getProvinces(listViewProvinces)
        }
    }
    Rectangle{
        z: 11
        id: recYN
        x: app.width * 0.3 / 2
        Behavior on y { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        y: app.height + 100 * app.dp
        height: app.height / 4
        width: app.width * 0.7
        color: palette.white_80_transparent


        Label{
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 30 * app.dp
            text: "XÁC NHẬN XÓA ?"
            font.pixelSize: 20 * app.dp
            color: palette.colorPrimary
        }

        Label{
            id: btnYN
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin:parent.height * 0.9
            width: parent.width
            Button
            {
                anchors.bottom: parent.bottom
                anchors.left: parent.left
                anchors.leftMargin: 10 * app.dp

                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        onDeleteCoupon()                        
                    }
                }
                Text
                {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 15 * app.dp
                    text: "Xóa"
                    color: palette.colorWhite
                }

                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: 100 * app.dp
                            implicitHeight: 50 * app.dp
                            color: "red"//palette.color_follow
                            radius: 80
                        }
                    }
            }
            Button
            {
                anchors.bottom: parent.bottom
                anchors.right: parent.right
                anchors.rightMargin: 10 * app.dp

                MouseArea{
                    anchors.fill: parent
                    onClicked: recYN_back()
                }
                Text
                {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 15 * app.dp
                    text: "Thoát"
                    color: palette.colorWhite
                }

                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: 100 * app.dp
                            implicitHeight: 50 * app.dp
                            color: palette.color_follow
                            //border.width: control.activeFocus ? 2 : 1
                            radius: 80
                        }
                    }
            }
        }
    }
    Rectangle{
        z: 11
        id: recOKGift
        x: app.width * 0.3 / 2
        Behavior on y { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        y: app.height + 100 * app.dp
        height: app.height / 3
        width: app.width * 0.7
        color: palette.white_80_transparent


        Label{
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 30 * app.dp
            text: "ĐỔI QUÀ THÀNH CÔNG"
            font.pixelSize: 20 * app.dp
            color: palette.colorPrimary
        }
        Text{
            anchors.verticalCenter: parent.verticalCenter
            anchors.top: parent.top
            anchors.topMargin: 60 * app.dp
            width: parent.width
            text: string.noti_received_gift
            font.pixelSize: 15 * app.dp
            wrapMode: Text.WordWrap
        }

        Label{
            id: btnOK
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin:parent.height * 0.9
            width: parent.width
            Button
            {
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter

                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        recOKGift.y = app.height + 100 * app.dp
                    }
                }
                Text
                {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 16 * app.dp
                    text: "OK"
                    color: palette.colorWhite
                }

                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: 100 * app.dp
                            implicitHeight: 50 * app.dp
                            color: palette.color_follow
                            radius: 80
                        }
                    }
            }
        }
    }
    Rectangle{
        id: receiveCouponView
        z: 10
        x: app.width * 0.1 / 2
        Behavior on y { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        y: app.height
        height: parent.height / 2
        width: parent.width * 0.9
        color: palette.white_80_transparent

        focus: true
        Keys.onBackPressed: receiveCouponView_back()

        Label{
            anchors.fill: parent
            Text{
                anchors.top: parent.top
                anchors.topMargin: 20 * app.dp
                anchors.horizontalCenter: parent.horizontalCenter
                text: "MÃ ƯU ĐÃI"
                font.pixelSize: 15 * app.dp
            }
            Text{
                anchors.top: parent.top
                anchors.topMargin: 50 * app.dp
                anchors.horizontalCenter: parent.horizontalCenter
                text: "XSDLDSMJ2623"
                font.pixelSize: 30 * app.dp
            }
            Image{
                id: imgQR
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                height: 100 * app.dp
                fillMode: Image.PreserveAspectFit
                source: "qrc:/images/qrcode-sample.jpg"
            }
        }

        Button
        {
            anchors.bottom: parent.bottom
            anchors.right: parent.right
            anchors.bottomMargin: 20 * app.dp
            anchors.rightMargin: 40 * app.dp
            MouseArea{
                anchors.fill: parent
                onClicked: onReceiveCouponSuccess()
            }
            Text
            {
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: 15 * app.dp
                text: "Nhận ưu đãi"
                color: palette.colorWhite
            }


            style: ButtonStyle {
                    background: Rectangle {
                        implicitWidth: 100 * app.dp
                        implicitHeight: 50 * app.dp
                        color: palette.color_follow
                        //border.width: control.activeFocus ? 2 : 1
                        radius: 80
                    }
                }
        }
        Button
        {
            anchors.bottom: parent.bottom
            anchors.left: parent.left
            anchors.bottomMargin: 20 * app.dp
            anchors.leftMargin: 40 * app.dp
            MouseArea{
                anchors.fill: parent
                onClicked: receiveCouponView_back()
            }
            Text
            {
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                font.pixelSize: 15 * app.dp
                text: "Hủy bỏ"
                color: palette.colorWhite
            }

            style: ButtonStyle {
                    background: Rectangle {
                        implicitWidth: 100 * app.dp
                        implicitHeight: 50 * app.dp
                        color: palette.color_follow
                        //border.width: control.activeFocus ? 2 : 1
                        radius: 80
                    }
                }
        }
    }
    Rectangle{
        z: 10
        id: receiveCouponSuccessView
        x: app.width * 0.1 / 2
        Behavior on y { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        y: app.height
        height: parent.height / 2
        width: parent.width * 0.9
        color: palette.white_80_transparent

        focus: true
        Keys.onBackPressed: {}

        Loader
        {
            anchors.fill: parent
            ScrollView {
             anchors.fill: parent
             flickableItem.interactive: false
                ListView
                {
                    property string nextUrl: null
                    property bool canAppend: true

                    anchors.fill: parent
                    property string cc: ""


                    id: listViewReceivedDealSuccess
                    delegate: ReceivedDealSuccessComponent{}
                    clip: true

                    property alias isEmpty: listViewReceivedDealSuccess_error.opa

                    ErrorComponent
                    {
                        id: listViewReceivedDealSuccess_error
                    }
                    Timer {
                        id: timer
                        interval: 1000; repeat: true
                        running: true
                        triggeredOnStart: true

                        onTriggered: {
                            var now = new Date ();
                            listViewReceivedDealSuccess.cc = now.getTime();

                          //  print(now.toISOString())
                        }
                    }
                }
             }
        }
      }


    Rectangle{
        id: notiView
        z: -1

        anchors.top: parent.top
        anchors.topMargin: menuHeight
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: menuHeight

        MouseArea {
            anchors.fill: parent
            onPressed: {
                           if(app.menuIsShown)
                               menuView_back()
                       }
        }
        ScrollView {
            id: scrollNotiView
         anchors.fill: parent
         flickableItem.interactive: true
         style: ScrollViewStyle {
                 transientScrollBars: true
                 handle: Item {
                     implicitWidth: 30
                     implicitHeight: 200
                     Rectangle {
                         color: palette.color_bg
                         anchors.fill: parent
                         anchors.topMargin: 6
                         anchors.leftMargin: 4
                         anchors.rightMargin: 4
                         anchors.bottomMargin: 6
                     }
                 }
                 scrollBarBackground: Item {
                     implicitWidth: 10
                     implicitHeight: 200
                 }
             }
            ListView
            {
                property string nextUrl: null
                property bool canAppend: true

                anchors.fill: parent

                id: listNotiView
                delegate: notiComponent
                clip: true
                model: ListModel{}
                onContentYChanged:
                {
                    if(scrollNotiView.flickableItem.visibleArea.yPosition > 0.8){
                        if(nextUrl !== null)
                            request(listNotiView, nextUrl)
                    }
                }

                property alias isEmpty: listNotiView_error.opa
                ErrorComponent
                {
                    id: listNotiView_error
                    property string text: string.empty_list_message
                }
            }
            Component.onCompleted: {
                    request(listNotiView, app.rootURL + "api/v1/message")
                    couponView.prevView = "noti"
            }
        }
    }



    Rectangle{
        id: profileView
        z: -1

        anchors.top: parent.top
        anchors.topMargin: menuHeight
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.bottomMargin: menuHeight


        ScrollView {
            id: scrollprofileView
         anchors.fill: parent
         flickableItem.interactive: false
         style: ScrollViewStyle {
                 transientScrollBars: true
                 handle: Item {
                     implicitWidth: 30
                     implicitHeight: 200
                     Rectangle {
                         color: palette.color_bg
                         anchors.fill: parent
                         anchors.topMargin: 6
                         anchors.leftMargin: 4
                         anchors.rightMargin: 4
                         anchors.bottomMargin: 6
                     }
                 }
                 scrollBarBackground: Item {
                     implicitWidth: 10
                     implicitHeight: 200
                 }
             }
            ListView
            {
                anchors.fill: parent

                id: listProfileView
                delegate: profileComponent
                clip: true
                model: ListModel{}
            }
            Component.onCompleted: {
                    //requestItem(listprofileView, app.rootURL + "api/v1/message")
            }
        }
    }
    ListModel
    {
        id: profileModel
        ListElement
        {
            //image: "http://storage.googleapis.com/jamja-prod/gcs_full_588089a076ec571d7df88025-2017-01-19-094049.png"

            points: 13000
            total: 0
        }
    }

    // receivedDealView
    Rectangle{
        id: receivedDealView
        width: parent.width
        height: parent.height
        x: app.width
        y: 0
        z: 1
        Behavior on x { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        color: palette.colorWhite

        property string deleteURL: null
        MouseArea
        {
            anchors.fill: parent
        }
        focus: true
        Keys.onBackPressed: receivedDealView_back()
        Rectangle {
            id: receivedDealView_menuBar
            z: 5
            anchors.top: parent.top
            anchors.topMargin: 0
            width: parent.width
            height: app.orientation == app.orientationLandscape ? 40*app.dp : menuHeight
            color: palette.colorWhite
            Rectangle {
                id: receivedDealView_menuButton
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                width: 1.2*height
                height: parent.height
                scale: receivedDealView_maMenuBar.pressed ? 1.2 : 1
                color: "transparent"
                Image {
                    anchors.centerIn: parent
                    height: parent.height * 0.5
                    fillMode: Image.PreserveAspectFit
                    source: "qrc:/images/ic_back_white_bg.png"
                }
                MouseArea {
                    id: receivedDealView_maMenuBar
                    anchors.fill: parent
                    onClicked: receivedDealView_back()
                }
                clip: true
            }
            Label {
                id: receivedDealView_titleText
                anchors.right: parent.right
                anchors.rightMargin: 100 * app.dp
                anchors.verticalCenter: parent.verticalCenter
                text: "Ưu đãi đã nhận"
                font.pixelSize: app.width / 20
                font.bold: true
                color: palette.colorPrimary
            }
        }
        Image {
            anchors.top: receivedDealView_menuBar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 6*app.dp
            z: 5
            source: "qrc:/images/shadow_title.png"
        }
        Rectangle{
            id: receivedDealViewMenu
            anchors.top: receivedDealView_menuBar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 50 * app.dp
            color: palette.colorPrimary
            Rectangle
            {
                anchors.left: parent.left
                anchors.top: parent.top
                width: parent.width * 0.3
                color: parent.color
                Text{
                    anchors.top: parent.top
                    anchors.topMargin: 15 * app.dp
                    anchors.verticalCenter: parent.verticalAlignment
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Thương hiệu"
                    color: palette.colorWhite
                    font.pixelSize: app.width / 25
                }
            }
            Rectangle
            {
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.leftMargin: parent.width * 0.3
                width: parent.width * 0.3
                color: parent.color
                Text{
                    anchors.top: parent.top
                    anchors.topMargin: 15 * app.dp
                    anchors.verticalCenter: parent.verticalAlignment
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Thời gian"
                    color: palette.colorWhite
                    font.pixelSize: app.width / 25
                }
            }
            Rectangle
            {
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.leftMargin: parent.width * 0.6
                width: parent.width * 0.2
                color: parent.color
                Text{
                    anchors.top: parent.top
                    anchors.topMargin: 15 * app.dp
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Điểm"
                    color: palette.colorWhite
                    font.pixelSize: app.width / 25
                }
            }
            Rectangle
            {
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.leftMargin: parent.width * 0.8
                width: parent.width * 0.2
                color: parent.color
                Text{
                    anchors.top: parent.top
                    anchors.topMargin: 15 * app.dp
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Lựa chọn"
                    color: palette.colorWhite
                    font.pixelSize: app.width / 25
                }
            }
        }
        Loader
        {
            anchors.top: receivedDealViewMenu.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom

            ScrollView {
                id: scrollReceivedDealView
             anchors.fill: parent
             flickableItem.interactive: true

             style: ScrollViewStyle {
                     transientScrollBars: true
                     handle: Item {
                         implicitWidth: 30
                         implicitHeight: 200
                         Rectangle {
                             color: palette.colorPrimary
                             anchors.fill: parent
                             anchors.topMargin: 6
                             anchors.leftMargin: 4
                             anchors.rightMargin: 4
                             anchors.bottomMargin: 6
                         }
                     }
                     scrollBarBackground: Item {
                         implicitWidth: 10
                         implicitHeight: 200
                     }
                 }
                ListView
                {
                    property string nextUrl: null
                    property bool canAppend: true

                    anchors.fill: parent
                    id: listViewReceivedDeal
                    delegate: receivedDealComponent
                    clip: true
                    model: ListModel{}

                    property alias isEmpty: listViewReceivedDeal_error.opa
                    ErrorComponent
                    {
                        id: listViewReceivedDeal_error
                        property string text: string.empty_received_deal
                    }
                }
               // Component.onCompleted: requestItem(listViewReceivedDealSuccess, "http://api.khuyenmai.re/api/v1/deal/" + 0 + "/show")
            }
        }
    }

    Rectangle{
        id: receivedDealHistoryView
        width: parent.width
        height: parent.height
        x: app.width
        y: 0
        z: 1
        Behavior on x { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        color: palette.colorWhite
        MouseArea
        {
            anchors.fill: parent
        }
        focus: true
        Keys.onBackPressed: receivedDealHistoryView_back()
        Rectangle {
            id: receivedDealHistoryView_menuBar
            z: 5
            anchors.top: parent.top
            anchors.topMargin: 0
            width: parent.width
            height: app.orientation == app.orientationLandscape ? 40*app.dp : menuHeight
            color: palette.colorWhite
            Rectangle {
                id: receivedDealHistoryView_menuButton
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                width: 1.2*height
                height: parent.height
                scale: receivedDealHistoryView_maMenuBar.pressed ? 1.2 : 1
                color: "transparent"
                Image {
                    anchors.centerIn: parent
                    height: parent.height * 0.5
                    fillMode: Image.PreserveAspectFit
                    source: "qrc:/images/ic_back_white_bg.png"
                }
                MouseArea {
                    id: receivedDealHistoryView_maMenuBar
                    anchors.fill: parent
                    onClicked: receivedDealHistoryView_back()
                }
                clip: true
            }
            Label {
                id: receivedDealHistoryView_titleText
                anchors.right: parent.right
                anchors.rightMargin: 100 * app.dp
                anchors.verticalCenter: parent.verticalCenter
                text: "Ưu đãi đã sử dụng"
                font.pixelSize: app.width / 20
                font.bold: true
                color: palette.colorPrimary
            }
        }
        Image {
            anchors.top: receivedDealHistoryView_menuBar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 6*app.dp
            z: 5
            source: "qrc:/images/shadow_title.png"
        }
        Rectangle{
            id: historyViewMenu
            anchors.top: receivedDealHistoryView_menuBar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 50 * app.dp
            color: palette.colorPrimary
            Rectangle
            {
                anchors.left: parent.left
                anchors.top: parent.top
                width: parent.width * 0.3
                color: parent.color
                Text{
                    anchors.top: parent.top
                    anchors.topMargin: 15 * app.dp
                    anchors.verticalCenter: parent.verticalAlignment
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Thương hiệu"
                    color: palette.colorWhite
                    font.pixelSize: app.width / 25
                }
            }
            Rectangle
            {
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.leftMargin: parent.width * 0.3
                width: parent.width * 0.3
                color: parent.color
                Text{
                    anchors.top: parent.top
                    anchors.topMargin: 15 * app.dp
                    anchors.verticalCenter: parent.verticalAlignment
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Thời gian sử dụng"
                    color: palette.colorWhite
                    font.pixelSize: app.width / 25
                }
            }
            Rectangle
            {
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.leftMargin: parent.width * 0.6
                width: parent.width * 0.2
                color: parent.color
                Text{
                    anchors.top: parent.top
                    anchors.topMargin: 15 * app.dp
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Điểm"
                    color: palette.colorWhite
                    font.pixelSize: app.width / 25
                }
            }
            Rectangle
            {
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.leftMargin: parent.width * 0.8
                width: parent.width * 0.2
                color: parent.color
                Text{
                    anchors.top: parent.top
                    anchors.topMargin: 15 * app.dp
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Lựa chọn"
                    color: palette.colorWhite
                    font.pixelSize: app.width / 25
                }
            }
        }
        Loader
        {
            anchors.top: historyViewMenu.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom

            ScrollView {
             anchors.fill: parent
             flickableItem.interactive: true

             style: ScrollViewStyle {
                     transientScrollBars: true
                     handle: Item {
                         implicitWidth: 30
                         implicitHeight: 200
                         Rectangle {
                             color: palette.colorPrimary
                             anchors.fill: parent
                             anchors.topMargin: 6
                             anchors.leftMargin: 4
                             anchors.rightMargin: 4
                             anchors.bottomMargin: 6
                         }
                     }
                     scrollBarBackground: Item {
                         implicitWidth: 10
                         implicitHeight: 200
                     }
                 }
                ListView
                {
                    property string nextUrl: null
                    property bool canAppend: true

                    anchors.fill: parent
                    id: listViewReceivedDealHistory
                    delegate: receivedDealHistoryComponent
                    clip: true
                    model: ListModel{}
                    property alias isEmpty: listViewReceivedDealHistory_error.opa
                    ErrorComponent
                    {
                        id: listViewReceivedDealHistory_error
                        property string text: string.empty_receive_history_deal
                    }
                }
               // Component.onCompleted: requestItem(listViewReceivedDealSuccess, "http://api.khuyenmai.re/api/v1/deal/" + 0 + "/show")

            }
        }

    }
    // GiftView
    Rectangle{
        id: giftView
        width: parent.width
        height: parent.height
        x: app.width
        y: 0
        z: 1
        Behavior on x { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        color: palette.colorWhite

        MouseArea
        {
            anchors.fill: parent
        }
        focus: true
        Keys.onBackPressed: giftView_back()
        Rectangle {
            id: giftView_menuBar
            z: 5
            anchors.top: parent.top
            anchors.topMargin: 0
            width: parent.width
            height: app.orientation == app.orientationLandscape ? 40*app.dp : menuHeight
            color: palette.colorWhite
            Rectangle {
                id: giftView_menuButton
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                width: 1.2*height
                height: parent.height
                scale: giftView_maMenuBar.pressed ? 1.2 : 1
                color: "transparent"
                Image {
                    anchors.centerIn: parent
                    height: parent.height * 0.5
                    fillMode: Image.PreserveAspectFit
                    source: "qrc:/images/ic_back_white_bg.png"
                }
                MouseArea {
                    id: giftView_maMenuBar
                    anchors.fill: parent
                    onClicked: giftView_back()
                }
                clip: true
            }
            Label {
                id: giftView_titleText
                anchors.right: parent.right
                anchors.rightMargin: 140 * app.dp
                anchors.verticalCenter: parent.verticalCenter
                text: "Đổi quà"
                font.pixelSize: app.width / 20
                font.bold: true
                color: palette.colorPrimary
            }
        }
        Image {
            anchors.top: giftView_menuBar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 6*app.dp
            z: 5
            source: "qrc:/images/shadow_title.png"
        }
        Rectangle{
            id: giftViewMenu
            anchors.top: giftView_menuBar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 50 * app.dp
            color: palette.colorPrimary
            Rectangle
            {
                anchors.left: parent.left
                anchors.top: parent.top
                width: parent.width * 0.5
                color: parent.color
                Text{
                    anchors.top: parent.top
                    anchors.topMargin: 15 * app.dp
                    anchors.verticalCenter: parent.verticalAlignment
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Quà tặng"
                    color: palette.colorWhite
                    font.pixelSize: app.width / 25
                }
            }
            Rectangle
            {
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.leftMargin: parent.width * 0.5
                width: parent.width * 0.2
                color: parent.color
                Text{
                    anchors.top: parent.top
                    anchors.topMargin: 15 * app.dp
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Điểm"
                    color: palette.colorWhite
                    font.pixelSize: app.width / 25
                }
            }
            Rectangle
            {
                anchors.left: parent.left
                anchors.top: parent.top
                anchors.leftMargin: parent.width * 0.7
                width: parent.width * 0.3
                color: parent.color
                Text{
                    anchors.top: parent.top
                    anchors.topMargin: 15 * app.dp
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    text: "Lựa chọn"
                    color: palette.colorWhite
                    font.pixelSize: app.width / 25
                }
            }
        }
        Loader
        {
            anchors.top: giftViewMenu.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom

            ScrollView {
                id: scrollGiftView
             anchors.fill: parent
             flickableItem.interactive: true

             style: ScrollViewStyle {
                     transientScrollBars: true
                     handle: Item {
                         implicitWidth: 30
                         implicitHeight: 200
                         Rectangle {
                             color: palette.colorPrimary
                             anchors.fill: parent
                             anchors.topMargin: 6
                             anchors.leftMargin: 4
                             anchors.rightMargin: 4
                             anchors.bottomMargin: 6
                         }
                     }
                     scrollBarBackground: Item {
                         implicitWidth: 10
                         implicitHeight: 200
                     }
                 }
                ListView
                {
                    property string nextUrl: null
                    property bool canAppend: true

                    anchors.fill: parent
                    id: listViewGift
                    delegate: GiftComponent{}
                    clip: true
                    model: ListModel{}
                    property alias isEmpty: listViewGift_error.opa
                    ErrorComponent
                    {
                        id: listViewGift_error
                        property string text: "Hiện tại chưa có quà đổi thưởng"
                    }
                }
               // Component.onCompleted: requestItem(listViewReceivedDealSuccess, "http://api.khuyenmai.re/api/v1/deal/" + 0 + "/show")
            }
        }
    }

    // GiftDetailView
    Rectangle{
        id: giftDetailView
        width: parent.width
        height: parent.height
        x: app.width
        y: 0
        z: 1
        Behavior on x { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        color: palette.colorWhite

        property string queryGiftDetail: "value"
        MouseArea
        {
            anchors.fill: parent
        }
        focus: true
        Keys.onBackPressed: giftDetailView_back()
        Rectangle {
            id: giftDetailView_menuBar
            z: 5
            anchors.top: parent.top
            anchors.topMargin: 0
            width: parent.width
            height: app.orientation == app.orientationLandscape ? 40*app.dp : menuHeight
            color: palette.colorWhite
            Rectangle {
                id: giftDetailView_menuButton
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                width: 1.2*height
                height: parent.height
                scale: giftDetailView_maMenuBar.pressed ? 1.2 : 1
                color: "transparent"
                Image {
                    anchors.centerIn: parent
                    height: parent.height * 0.5
                    fillMode: Image.PreserveAspectFit
                    source: "qrc:/images/ic_back_white_bg.png"
                }
                MouseArea {
                    id: giftDetailView_maMenuBar
                    anchors.fill: parent
                    onClicked: giftDetailView_back()
                }
                clip: true
            }
            Label {
                id: giftDetailView_titleText
                //anchors.right: parent.right
               // anchors.rightMargin: 140 * app.dp
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Thông tin đổi thưởng"
                font.pixelSize: app.width / 20
                font.bold: true
                color: palette.colorPrimary
            }
        }
        Image {
            anchors.top: giftDetailView_menuBar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 6*app.dp
            z: 5
            source: "qrc:/images/shadow_title.png"
        }
        Loader
        {
            anchors.top: giftDetailView_menuBar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom

            ScrollView {
                id: scrollGiftDetailView
             anchors.fill: parent
             flickableItem.interactive: true

             style: ScrollViewStyle {
                     transientScrollBars: true
                     handle: Item {
                         implicitWidth: 30
                         implicitHeight: 200
                         Rectangle {
                             color: palette.colorPrimary
                             anchors.fill: parent
                             anchors.topMargin: 6
                             anchors.leftMargin: 4
                             anchors.rightMargin: 4
                             anchors.bottomMargin: 6
                         }
                     }
                     scrollBarBackground: Item {
                         implicitWidth: 10
                         implicitHeight: 200
                     }
                 }
                ListView
                {
                    property string nextUrl: null
                    property bool canAppend: true

                    anchors.fill: parent
                    id: listViewGiftDetail
                    delegate: GiftDetailComponent{}
                    clip: true
                    model: ListModel{}
                    property alias isEmpty: listViewGiftDetail_error.opa
                    ErrorComponent
                    {
                        id: listViewGiftDetail_error
                    }
                }
               // Component.onCompleted: requestItem(listViewReceivedDealSuccess, "http://api.khuyenmai.re/api/v1/deal/" + 0 + "/show")
            }
        }
    }
    Rectangle{
        id: infoUserView
        width: parent.width
        height: parent.height
        x: app.width
        y: 0
        z: 1
        Behavior on x { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        color: palette.colorWhite
        MouseArea
        {
            anchors.fill: parent
        }
        focus: true
        Keys.onBackPressed: infoUserView_back()
        Rectangle {
            id: infoUserView_menuBar
            z: 5
            anchors.top: parent.top
            anchors.topMargin: 0
            width: parent.width
            height: app.orientation == app.orientationLandscape ? 40*app.dp : menuHeight
            color: palette.colorWhite
            Rectangle {
                id: infoUserView_menuButton
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                width: 1.2*height
                height: parent.height
                scale: infoUserView_maMenuBar.pressed ? 1.2 : 1
                color: "transparent"
                Image {
                    anchors.centerIn: parent
                    height: parent.height * 0.5
                    fillMode: Image.PreserveAspectFit
                    source: "qrc:/images/ic_back_white_bg.png"
                }
                MouseArea {
                    id: infoUserView_maMenuBar
                    anchors.fill: parent
                    onClicked: infoUserView_back()
                }
                clip: true
            }
            Label {
                id: infoUserView_titleText
                anchors.right: parent.right
                anchors.rightMargin: 100 * app.dp
                anchors.verticalCenter: parent.verticalCenter
                text: "Thông tin cá nhân"
                font.pixelSize: app.width / 20
                font.bold: true
                color: palette.colorPrimary
            }
        }
        Image {
            anchors.top: infoUserView_menuBar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 6*app.dp
            z: 5
            source: "qrc:/images/shadow_title.png"
        }
        Loader
        {
            anchors.top: infoUserView_menuBar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom

            ScrollView {
             anchors.fill: parent
             flickableItem.interactive: false

             style: ScrollViewStyle {
                     transientScrollBars: true
                     handle: Item {
                         implicitWidth: 30
                         implicitHeight: 200
                         Rectangle {
                             color: palette.colorPrimary
                             anchors.fill: parent
                             anchors.topMargin: 6
                             anchors.leftMargin: 4
                             anchors.rightMargin: 4
                             anchors.bottomMargin: 6
                         }
                     }
                     scrollBarBackground: Item {
                         implicitWidth: 10
                         implicitHeight: 200
                     }
                 }
                ListView
                {
                    property string nextUrl: null
                    property bool canAppend: true

                    anchors.fill: parent
                    id: listViewInfoUser
                    delegate: InfoUserComponent{}
                    clip: true
                    model: ListModel{}
                }
            }
        }

    }

    Rectangle
    {
        id: recChangePasswordView
        //anchors.bottom: parent.bottom
        height: app.height
        width: parent.width
        color: palette.colorWhite
        y: app.height
        x: 0
        z: 10
        Behavior on y { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        property alias error: lbError.text
        property alias password: tfPassword.text
        property alias username: tfUsername.text
        MouseArea
        {
            anchors.fill: parent
        }

        focus: true
        Keys.onBackPressed: recChangePasswordView_back()
        Text
        {
            anchors.top: parent.top
            anchors.margins: 30 * app.dp
            anchors.horizontalCenter: parent.horizontalCenter
            text: "ĐỔI MẬT KHẨU"
            font.pixelSize: 30 * app.dp
            color: palette.colorPrimary
            font.bold: true
        }

        TextField
        {
            id: tfChangePassword
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 100 * app.dp
            placeholderText: "Nhập mật khẩu cũ"
            text: ""
            readOnly: tfChangeNPassword1.visible
            height: 50 * app.dp
            width: app.width * 0.8
            font.pixelSize: 15 * app.dp
            font.bold: true
            maximumLength: 14
            echoMode: TextInput.Password
            horizontalAlignment: TextInput.AlignHCenter
            style: TextFieldStyle {
                    textColor: palette.colorPrimary
                    background: Rectangle {
                        radius: 5
                        //implicitWidth: 100
                        //implicitHeight: 24
                        border.color: tfChangePassword.readOnly ? palette.color_bg : palette.colorPrimary
                        border.width: 5
                    }
                }
        }

        TextField
        {
            id: tfChangeNPassword1
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: tfChangePassword.bottom
            anchors.topMargin: (height === 0) ?  0 : 20 * app.dp
            placeholderText: "Nhập mật khẩu mới"

            visible: false
            height: visible ? 50 * app.dp : 0
            Behavior on height { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

            text: ""
            width: app.width * 0.8
            font.pixelSize: 15 * app.dp
            font.bold: true
            maximumLength: 14
            echoMode: TextInput.Password
            horizontalAlignment: TextInput.AlignHCenter
            style: TextFieldStyle {
                    textColor: palette.colorPrimary
                    background: Rectangle {
                        radius: 5
                        //implicitWidth: 100
                        //implicitHeight: 24
                        border.color: tfChangeNPassword1.readOnly ? palette.color_bg : palette.colorPrimary
                        border.width: 5
                    }
                }
        }
        TextField
        {
            id: tfChangeNPassword2
            anchors.horizontalCenter: parent.horizontalCenter
             anchors.top: tfChangeNPassword1.bottom
            anchors.topMargin: (height === 0) ?  0 : 20 * app.dp
            placeholderText: "Nhập lại mật khẩu mới"

            visible: false
            height: visible ? 50 * app.dp : 0
            Behavior on height { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }


            text: ""
            width: app.width * 0.8
            font.pixelSize: 15 * app.dp
            font.bold: true
            maximumLength: 14
            echoMode: TextInput.Password
            horizontalAlignment: TextInput.AlignHCenter
            style: TextFieldStyle {
                    textColor: palette.colorPrimary
                    background: Rectangle {
                        radius: 5
                        //implicitWidth: 100
                        //implicitHeight: 24
                        border.color: tfChangeNPassword1.readOnly ? palette.color_bg : palette.colorPrimary
                        border.width: 5
                    }
                }
        }


        Label{
            id: lbChangePassError
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: tfChangeNPassword2.bottom
            anchors.margins: 5 * app.dp
            width: app.width * 0.7
            text: parent.error
            font.pixelSize: 10 * app.dp
            color: palette.colorPrimary
        }


        Label{
            anchors.right: parent.right
            anchors.top: tfChangeNPassword2.bottom
            anchors.margins: 20 * app.dp
            width: app.width * 0.7

            Button
            {
                anchors.right: parent.right
                MouseArea{
                    anchors.fill: parent

                    onClicked:{
                        if(!tfChangeNPassword1.visible)
                        {
                            var url = "http://api.khuyenmai.re/api/v1/profile/checked?password=" + tfChangePassword.text
                            checkSuccess(url, function (o) {
                                                    var d = o.responseText;
                                if(d !== "")
                                {
                                    if(d === "0")
                                        lbChangePassError.text = "*Mật khẩu không đúng"
                                    else
                                    {
                                       lbChangePassError.text = ""
                                       tfChangeNPassword1.visible = true
                                       tfChangeNPassword1.text = ""
                                       tfChangeNPassword2.visible = true
                                       tfChangeNPassword2.text = ""                                        
                                    }
                                }
                            });
                        }

                        else
                        {
                            if(tfChangeNPassword1.text !== tfChangeNPassword2.text)
                                lbChangePassError.text = "*Mật khẩu không trùng nhau"
                            else if(!rightPassword(tfChangeNPassword1.text))
                                lbChangePassError.text = "*Mật khẩu lớn hơn 6 kí tự, bao gồm A-Z a-z 0-9"
                            else if(tfChangeNPassword1.text === tfChangePassword.text)
                                lbChangePassError.text = "*Mật khẩu mới phải khác mật khẩu cũ"
                            else
                            {
                                url = "http://api.khuyenmai.re/api/v1/profile/changePassword?password=" + tfChangePassword.text + "&newPassword=" + tfChangeNPassword1.text
                                post(url)
                                print(url)
                                recChangePasswordView_back()
                                tfChangeNPassword1.visible = false
                                tfChangeNPassword1.text = ""
                                tfChangeNPassword2.visible = false
                                tfChangeNPassword2.text = ""
                                tfChangePassword.text = ""
                                lbChangePassError.text = ""
                            }
                        }

                    }
                }
                Text
                {
                    id: btnChangePassNext
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 15 * app.dp
                    text: "Tiếp theo"
                    color: palette.colorWhite
                }


                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: 100 * app.dp
                            implicitHeight: 50 * app.dp
                            color: palette.color_follow
                            //border.width: control.activeFocus ? 2 : 1
                            radius: 80
                        }
                    }
            }
            Button
            {
                anchors.right: parent.right
                anchors.rightMargin: 110 * app.dp
                MouseArea{
                    anchors.fill: parent
                    onClicked: recChangePasswordView_back()
                }
                Text
                {
                    id: btnChangePassCancel
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 15 * app.dp
                    text: "Hủy"
                    color: palette.colorWhite
                }


                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: 100 * app.dp
                            implicitHeight: 50 * app.dp
                            color: palette.color_follow
                            //border.width: control.activeFocus ? 2 : 1
                            radius: 80
                        }
                    }
            }
            Button
            {
                visible: tfChangeNPassword1.visible
                anchors.right: parent.right
                anchors.rightMargin: 220 * app.dp
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        lbChangePassError.text = ""
                        if(tfChangeNPassword1.visible)
                        {
                            tfChangeNPassword1.visible = false
                            tfChangeNPassword2.visible = false
                            lbChangePassError.text = ""
                        }
                    }
                }
                Text
                {
                    id: btnChangePassPrev
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 15 * app.dp
                    text: "Quay lại"
                    color: palette.colorWhite
                }


                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: 100 * app.dp
                            implicitHeight: 50 * app.dp
                            color: palette.color_follow
                            //border.width: control.activeFocus ? 2 : 1
                            radius: 80
                        }
                    }
            }
        }
    }

    // About
    Rectangle{
        id: aboutView
        width: parent.width
        height: parent.height
        x: app.width
        y: 0
        z: 1
        Behavior on x { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        color: palette.colorWhite

        MouseArea
        {
            anchors.fill: parent
        }
        focus: true
        Keys.onBackPressed: aboutView_back()

        Rectangle {
            id: aboutView_menuBar
            z: 5
            anchors.top: parent.top
            anchors.topMargin: 0
            width: parent.width
            height: app.orientation == app.orientationLandscape ? 40*app.dp : menuHeight
            color: palette.colorWhite
            Rectangle {
                anchors.left: parent.left
                anchors.verticalCenter: parent.verticalCenter
                width: 1.2*height
                height: parent.height
                scale: aboutView_maMenuBar.pressed ? 1.2 : 1
                color: "transparent"
                Image {
                    anchors.centerIn: parent
                    height: parent.height * 0.5
                    fillMode: Image.PreserveAspectFit
                    source: "qrc:/images/ic_back_white_bg.png"
                }
                MouseArea {
                    id: aboutView_maMenuBar
                    anchors.fill: parent
                    onClicked: aboutView_back()
                }
                clip: true
            }
            Label {
                //anchors.right: parent.right
               // anchors.rightMargin: 140 * app.dp
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Thông tin KhuyenmaiRe"
                font.pixelSize: 0.4*parent.height
                font.bold: true
                color: palette.colorPrimary
            }
        }
        Image {
            anchors.top: aboutView_menuBar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 6*app.dp
            z: 5
            source: "qrc:/images/shadow_title.png"
        }
        Image {
            id: aboutImg
            source: "qrc:/images/ph_logo.png"
            //anchors.verticalCenter: parent.verticalCenter
           // anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: aboutView_menuBar.bottom
            anchors.left: parent.left
            anchors.margins: 20 * app.dp
            anchors.right: parent.right
            height: 100 * app.dp
            fillMode: Image.PreserveAspectFit
            antialiasing: true
            smooth: true
            //anchors.top: parent.top
            //anchors.left: parent.left
            //opacity: 0.5
        }
        Label
        {
            anchors.top: aboutImg.bottom
            anchors.left: parent.left
            anchors.right: parent.right

            Text
            {
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right

                anchors.margins: 10 * app.dp
                text: "Phiên bản hiện tại: " + app.version + string.about_khuyenmaire
                font.pixelSize: 15 * app.dp
                wrapMode: Text.WordWrap
               // color: "black"
            }
        }

        Loader
        {
            anchors.top: giftDetailView_menuBar.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
        }
    }
}

