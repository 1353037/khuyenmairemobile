import QtQuick 2.0

import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.0
Component{
        Rectangle {
            anchors.fill:parent
            property int  time: modelData.secondDiff
            Text{
                anchors.top: parent.top
                anchors.topMargin: 20 * app.dp
                anchors.horizontalCenter: parent.horizontalCenter
                text: "THÔNG TIN ƯU ĐÃI"
                font.pixelSize: 20 * app.dp
                color: palette.colorPrimary
            }
            Text{
                anchors.top: parent.top
                anchors.topMargin: 50 * app.dp
                anchors.horizontalCenter: parent.horizontalCenter
                text: modelData.coupon_code
                font.pixelSize: 30 * app.dp
            }
            Text{
                id: coupon_time
                anchors.top: parent.top
                anchors.topMargin: 90 * app.dp
                anchors.horizontalCenter: parent.horizontalCenter
                text:  (parent.time > 0) ? "Còn " + getHour(parent.time) + "h" + getMinute(parent.time) + "m" + getSecond(parent.time) + "s  (+" + modelData.points + "đ)": "Hết hạn"
                color: (parent.time > 0) ? "green" : "red"
                font.bold: true
            }

            Timer {
                id: timer
                interval: 1000; repeat: true
                running: true
                triggeredOnStart: true
                onTriggered: {
                    parent.time = parseInt(parent.time) - 1
                }
            }
            Image{
                id: imgQR2
                anchors.top: parent.top
                anchors.topMargin: 120 * app.dp
                anchors.horizontalCenter: parent.horizontalCenter
                height: 100 * app.dp
                fillMode: Image.PreserveAspectFit
                source: "qrc:/images/qrcode-sample.jpg"
            }

            Button
            {
                anchors.top: parent.top
                anchors.topMargin: 220 * app.dp
                anchors.horizontalCenter: parent.horizontalCenter
                MouseArea{
                    anchors.fill: parent
                    onClicked: receiveCouponSuccessView_back()
                }
                Text
                {
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 15 * app.dp
                    text: "OK"
                    color: palette.colorWhite
                }


                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: 100 * app.dp
                            implicitHeight: 50 * app.dp
                            color: palette.color_follow
                            //border.width: control.activeFocus ? 2 : 1
                            radius: 80
                        }
                    }
            }
        }
}
