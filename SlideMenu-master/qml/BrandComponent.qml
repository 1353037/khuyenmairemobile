import QtQuick 2.0
import QtQuick.Controls 1.2
//import QtQuick.Controls 2.1
import QtQuick.Controls.Styles 1.4
Component
{
    Item
    {
        anchors.top: app.top
        anchors.right: app.right
        anchors.left: app.left
        height: app.height * 1.8
        width: app.width
        Rectangle
        {
            id: brandDetail
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.right: parent.right
            height: parent.height * 0.2
            //color: palette.colorPrimary

            Image
            {
                id: imgBrandLogo
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
                anchors.topMargin: parent.height * 0.1
                //anchors.rightMargin: app.dp * 60
                //anchors.alignWhenCentered: true
                height: parent.height*0.3
                width: parent.width / 2
                fillMode: Image.PreserveAspectFit
                source: app.prefixLogo(modelData.logo)
                //source: "http://storage.googleapis.com/jamja-prod/gcs_full_588089a076ec571d7df88025-2017-01-19-094049.png"
                smooth: true
                antialiasing: true
            }

            Text
            {
                id: textBrand
                anchors.top: imgBrandLogo.bottom
                anchors.topMargin: parent.height * 0.05
                //anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter

                text: modelData.brand_name

                font.pixelSize: parent.height*0.1
                color: palette.colorPrimary
            }
/*
            Button
            {
                anchors.top: textBrand.bottom
                anchors.topMargin: parent.height * 0.01
                //anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                //color: third
                //radius: 8
                Image
                {
                    anchors.left: parent.left
                    anchors.leftMargin: parent.height * 0.3
                    height: parent.height*0.5
                    fillMode: Image.PreserveAspectFit
                    anchors.verticalCenter: parent.verticalCenter
                    source: "qrc:/images/ic_followed_white.png"
                }
                Text
                {
                    anchors.right: parent.right
                    anchors.rightMargin: parent.height * 0.3
                    anchors.verticalCenter: parent.verticalCenter
                    font.pixelSize: parent.height*0.3
                    text: string.follow
                    color: palette.colorWhite
                }


                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: 120 * app.dp
                            implicitHeight: 50 * app.dp
                            color: palette.color_follow
                            //border.width: control.activeFocus ? 2 : 1
                            //border.color: "#888"
                            radius: 80
                    }
                }
            }*/
        }

        Label
        {
            id: brandDetail_description
            anchors.top: brandDetail.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: text.bottom
            anchors.margins: app.dp * 8
            text: (modelData.description === null) ? '' : '<b>' + modelData.brand_name + ': </b>' + modelData.description
            font.pixelSize: app.dp * 20
            wrapMode: Text.WordWrap
        }

        Image{
            id: brandDetail_promotion
            anchors.top: brandDetail_description.bottom
            anchors.topMargin: parent.height * 0.02
            anchors.left: parent.left
            anchors.right: parent.right
            height: parent.height * 0.1
            source: "qrc:/images/promote.jpg"
            fillMode: Image.PreserveAspectCrop
            smooth: true
            antialiasing: true
        }

        TabView {
            id: frame
            anchors.top: brandDetail_promotion.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.topMargin: 20

            Tab {
                title: string.benifit
                Rectangle
                {
                    anchors.fill: parent
                    ScrollView
                    {
                        id: scrollBenefit
                        anchors.fill: parent
                        flickableItem.interactive: true

                        style: ScrollViewStyle {
                                transientScrollBars: true
                                handle: Item {
                                    implicitWidth: 30
                                    implicitHeight: 200
                                    Rectangle {
                                        color: palette.color_bg
                                        anchors.fill: parent
                                        anchors.topMargin: 6
                                        anchors.leftMargin: 4
                                        anchors.rightMargin: 4
                                        anchors.bottomMargin: 6
                                    }
                                }
                                scrollBarBackground: Item {
                                    implicitWidth: 10
                                    implicitHeight: 200
                                }
                            }
                        ListView{
                            id: listViewBenefit
                            property string nextUrl: ""
                            property bool canAppend: true

                            model: ListModel{}
                            delegate: offerComponent
                            clip: true
                            onContentYChanged:
                            {
                                if(scrollBenefit.flickableItem.visibleArea.yPosition > 0.8){
                                    if(nextUrl !== null)
                                        request(listViewBenefit, nextUrl)
                                }
                            }
                            property alias isEmpty: listViewBenefit_error.opa
                            ErrorComponent
                            {
                                id: listViewBenefit_error
                                property string text: string.empty_list_store
                            }

                            Component.onCompleted:
                            {
                                couponView.prevView = "brand"
                                request(listViewBenefit, app.rootURL + "api/v1/deal?brand_id=" + modelData.id)
                            }
                        }
                    }
                }
            }
            Tab {
                title: string.store_location

                Rectangle
                {
                    anchors.fill: parent

                    ScrollView
                    {
                        id: scrollLocation
                        anchors.fill: parent
                        flickableItem.interactive: true

                        style: ScrollViewStyle {
                                transientScrollBars: true
                                handle: Item {
                                    implicitWidth: 30
                                    implicitHeight: 200
                                    Rectangle {
                                        color: palette.color_bg
                                        anchors.fill: parent
                                        anchors.topMargin: 6
                                        anchors.leftMargin: 4
                                        anchors.rightMargin: 4
                                        anchors.bottomMargin: 6
                                    }
                                }
                                scrollBarBackground: Item {
                                    implicitWidth: 10
                                    implicitHeight: 200
                                }
                            }
                        ListView{
                            id: listViewLocation
                            property string nextUrl: ""
                            property bool canAppend: true

                            spacing: 1
                            model: ListModel{}
                            delegate:
                                Rectangle
                                {
                                    width: parent.width
                                    height: 40 * app.dp
                                    color: maBrandAddressRec.pressed ? palette.color_bg : palette.colorWhite
                                    Rectangle{
                                        anchors.bottom: parent.bottom
                                        width: parent.width
                                        height: 1
                                        color: palette.color_bg
                                    }
                                    MouseArea
                                    {
                                        id: maBrandAddressRec
                                        anchors.fill: parent
                                    }

                                    Label
                                    {
                                        anchors.top: parent.top
                                        anchors.left: parent.left
                                        anchors.right: parent.right
                                        anchors.leftMargin: 15 * app.dp
                                        anchors.topMargin: 5 * app.dp
                                        Image
                                        {
                                            id: icAddress
                                            anchors.left: parent.left
                                            fillMode: Image.PreserveAspectFit
                                            source: "qrc:/images/ic_offer_navigation.png"
                                            smooth: true
                                            antialiasing: true
                                            height: 15 * app.dp
                                        }

                                        Text
                                        {
                                            anchors.left: icAddress.right
                                            text: prefixAddress(address)
                                            font.pixelSize: 13  * app.dp
                                        }
                                    }
                                    Label
                                    {
                                        anchors.bottom: parent.bottom
                                        anchors.left: parent.left
                                        anchors.right: parent.right
                                        anchors.leftMargin: 20 * app.dp
                                        Image
                                        {
                                            id: icPhoneNum
                                            height: 13 * app.dp
                                            fillMode: Image.PreserveAspectFit
                                            source: "qrc:/images/ic_offer_phone.png"
                                            smooth: true
                                            antialiasing: true
                                            visible: (phone_number === "") ? false : true
                                        }
                                        Text
                                        {
                                            anchors.left: icPhoneNum.right
                                            text: phone_number
                                            font.pixelSize: 10 * app.dp
                                        }
                                    }
                                    Image{
                                        scale: maBrandAddress.pressed ? 1.2 : 1
                                        anchors.right: parent.right
                                        anchors.rightMargin: 20 * app.dp
                                        anchors.verticalCenter: parent.verticalCenter
                                        height: 20 * app.dp
                                        width: height
                                        fillMode: Image.PreserveAspectFit
                                        source: "qrc:/images/ic_more.png"

                                        MouseArea {
                                            id: maBrandAddress
                                            anchors.fill: parent
                                            onClicked: {
                                                app.latitude = latitude
                                                app.longitude = longitude
                                                //requestItem(listViewBrand, "http://api.khuyenmai.re/api/v1/brand/?query=" + brand_name)
                                                app.address = address
                                                locationView.prevView = "brand"
                                                onLocation()
                                            }
                                        }
                                    }
                                }

                            clip: true
                            onContentYChanged:
                            {
                                if(scrollLocation.flickableItem.visibleArea.yPosition > 0.8){
                                    if(nextUrl !== null)
                                        request(listViewLocation, nextUrl)
                                }
                            }
                            property alias isEmpty: listViewLocation_error.opa
                            ErrorComponent
                            {
                                id: listViewLocation_error
                            }

                            Component.onCompleted:
                            {
                                request(listViewLocation, app.rootURL + "api/v1/brand/" + modelData.id +"/detail/store")
                            }
                        }
                    }
                }
            }
            Tab { title: string.comment
                Rectangle
                {
                    anchors.fill: parent
                    color: palette.color_bg
                }
            }
            style: TabViewStyle {
                tabsAlignment: Qt.AlignHCenter
                    frameOverlap: 1
                    tab: Rectangle {
                        color: styleData.selected ? palette.colorPrimary : palette.colorWhite
                        border.color:  palette.colorPrimary
                        implicitWidth: (app.width - 50 * app.dp) / 3
                        implicitHeight: 40 * app.dp
                        radius: 1
                        Text {
                            id: text
                            anchors.centerIn: parent
                            text: styleData.title
                            color: styleData.selected ? palette.colorWhite : palette.colorPrimary
                            font.pixelSize: app.dp * 20
                        }
                    }
                }
        }
    }

}
