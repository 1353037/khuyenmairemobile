import QtQuick 2.0

Rectangle
                {
                    height: searchHeight
                    width: app.width / 4

                    Text
                    {
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                        text: category_name
                        font.pixelSize: parent.height * 0.3
                    }
                }
