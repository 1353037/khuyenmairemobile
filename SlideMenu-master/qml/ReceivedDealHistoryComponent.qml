import QtQuick 2.0
import QtQuick.Controls 1.2
//import QtQuick.Controls 2.1
import QtQuick.Controls.Styles 1.4
Component
{
    Rectangle
    {
        height: app.menuHeight
        width: parent.width
        color: maReceivedDeal.pressed ? palette.color_bg : palette.colorWhite
        Rectangle{
            anchors.bottom: parent.bottom
            color: palette.color_bg
            height: 1
            width: parent.width
        }
        MouseArea{
            id: maReceivedDeal
            anchors.fill: parent
        }

        Image{
            anchors.left: parent.left
            anchors.margins: 10 * app.dp
            //anchors.leftMargin: parent.width * 0.4
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width * 0.3
            height: parent.height
            source: app.prefixLogo(brand.logo)
            fillMode: Image.PreserveAspectFit
            smooth: true
            antialiasing: true
        }
        Label{
            id: lbCouponCode
            anchors.left: parent.left
            anchors.leftMargin: parent.width * 0.4
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width * 0.3
            Text{
                anchors.fill: parent
                color: "green"
                text: app.prefixDate(last_modified)
            }
        }
        Label{
            id: lbPoint
            anchors.left: parent.left
            anchors.leftMargin: parent.width * 0.65
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: Text.AlignHCenter
            width: parent.width * 0.2
            Text{
                anchors.fill: parent
                color: "green"
                text: "+" + points + "đ"
            }
        }
        Image{
            anchors.right: parent.right
            anchors.rightMargin: 20 * app.dp
            anchors.verticalCenter: parent.verticalCenter
            width: 20 * app.dp
            height: 20 * app.dp
            source: "qrc:/images/more_detail_ic.png"
            fillMode: Image.PreserveAspectFit
            smooth: true
            antialiasing: true
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    couponView.prevView = "receivedHistory"
                    couponView.logoURL = app.prefixLogo(brand.logo)
                    requestItem(listViewCoupon, "http://api.khuyenmai.re/api/v1/deal/" + deal_id + "/detail")
                    onCoupon()
                }
            }
        }



/*
        Image{
            anchors.left: parent.left
            anchors.leftMargin: parent.width / 0.3
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width / 0.2
            source: brand_logo
            fillMode: Image.PreserveAspectFit
            smooth: true
            antialiasing: true
        }*/
    }

}
