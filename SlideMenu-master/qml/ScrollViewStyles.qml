import QtQuick 2.0

import QtQuick.Window 2.2
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.3
import QtPositioning 5.3
import QtLocation 5.5
import QtQuick.Controls.Private 1.0
ScrollViewStyle {
                     transientScrollBars: true
                     handle: Item {
                         implicitWidth: 10
                         implicitHeight: 50
                         Rectangle {
                             color: palette.color_bg
                             anchors.fill: parent
                             anchors.topMargin: searchHeight + 6
                             anchors.leftMargin: 4
                             anchors.rightMargin: 4
                             anchors.bottomMargin: 6
                         }
                     }
                     scrollBarBackground: Item {
                         implicitWidth: 10
                         implicitHeight: 50
                     }
                 }
