import QtQuick 2.0

Component
{
    Rectangle
    {
        id: re
        anchors.left: parent.left
        anchors.right: parent.right

        property int time: secondDiff
        height: (time > 0) ? 200 * app.dp : 0
        color: palette.colorWhite
        //scale: maOffer.pressed ? 1.2 : 1
        visible: (time > 0) ? true : false
        Behavior on visible { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }
        Behavior on height { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

        Image
        {
            id: imgImage
            //anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.right: parent.right
            height: parent.height * 0.75
            fillMode: Image.PreserveAspectCrop
            //fillMode: Image.PreserveAspectFit
            source: app.prefixLogo(thumb)
            smooth: true
            antialiasing: true
        }
        Text
        {
            z: 1
            id: hotText
            anchors.right: imgImage.right
            anchors.rightMargin: parent.height * 0.04
            anchors.top: imgImage.top
            anchors.topMargin: parent.height * 0.02
            font.pixelSize: parent.height * 0.07
            text: highlight
            color: palette.colorWhite
        }
        Rectangle
        {
            z: 0
            anchors.top: parent.top
            anchors.left: hotText.left

            anchors.leftMargin: parent.height * -0.02
            anchors.rightMargin: parent.height * -0.02
            anchors.bottomMargin: parent.height * -0.02

            anchors.right: parent.right
            anchors.bottom: hotText.bottom
            color: palette.colorPrimary
            visible: highlight !== "" ? true : false
        }

        Image
        {
            z: 1
            id: starImage
            //anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: parent.height * 0.05
            anchors.top: parent.top
            anchors.topMargin: parent.height * 0.07

            height: parent.height * 0.07
            fillMode: Image.PreserveAspectFit
            //fillMode: Image.PreserveAspectFit
            source: "qrc:/images/ic_rated.png"
            smooth: true
            antialiasing: true
        }
        Text
        {
            z: 1
            id: starText
            anchors.left: starImage.right
            anchors.leftMargin: 5 * app.dp
            anchors.top: parent.top
            anchors.topMargin: 10 * app.dp
            font.pixelSize: 16 * app.dp
            //anchors.verticalCenter: parent.verticalCenter
            //anchors.margins: parent.height * 0.05
            text: rated
            color: palette.colorWhite
        }

        Rectangle
        {
            z: 0
            id: recStar
            anchors.left: starImage.left
            anchors.right: starText.right
            anchors.top: starImage.top
            anchors.bottom: starImage.bottom

            anchors.margins: parent.height * -0.02
            color: palette.black_30_transparent
        }

        Text
        {
            anchors.top: imgImage.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            //anchors.verticalCenter: parent.verticalCenter
            anchors.margins: parent.height * 0.02
            text: title
            font.pixelSize: parent.height * 0.08

            wrapMode: Text.WordWrap
        }

        Rectangle
        {
            id: recDate
            anchors.left: parent.left
            anchors.bottom: imgImage.bottom
            anchors.bottomMargin: parent.height * 0.03
            height: 25 * app.dp
            width: 90 * app.dp
            color: palette.black_30_transparent
        }
        Text
        {
            anchors.top: recDate.top
            anchors.right: recDate.right
            anchors.bottom: recDate.bottom

            anchors.margins:  parent.height * 0.03
            //anchors.left: recDate.left

            //anchors.verticalCenter: parent.verticalCenter
            //anchors.horizontalCenter: parent.horizontalCenter
            //anchors.horizontalCenter: Text.horizontalAlignment
            text: (getDay(parent.time) >= 1) ? app.prefixDate(end_date) :
                                               ((getHour(parent.time) >= 1) ? "Còn " + getHour(parent.time) + " giờ" :
                                                                               ((getMinute(parent.time) >= 1) ? "Còn " + getMinute(parent.time) + " phút" :
                                                                                                               "Còn " + getSecond(parent.time) + " giây"))
            font.pixelSize: 10 * app.dp
            font.italic: true
            color: palette.colorWhite
        }
        Timer {
            id: timer
            interval: 1000; repeat: true
            running: true
            triggeredOnStart: true
            onTriggered: {
                parent.time = parseInt(parent.time) - 1
            }
        }
        Image
        {
            anchors.left: recDate.left
            anchors.bottom: recDate.bottom
            anchors.top: recDate.top
            anchors.margins:  2 * app.dp
            anchors.bottomMargin: 6 * app.dp
            anchors.leftMargin: 6 * app.dp
           // anchors.leftMargin: parent.height * 0.02
            height: 3 * app.dp
            fillMode: Image.PreserveAspectFit
            //anchors.verticalCenter: parent.verticalCenter
            source: "qrc:/images/ic_offer_expiry_dark.png"
        }
        Image{
            id: recFavou
            anchors.right: parent.right
            anchors.bottom: imgImage.bottom
            anchors.margins: 10 * app.dp
            scale: maDealFav.pressed ? 1.2 : 1
            height: 40 * app.dp
            width: 40 * app.dp
            fillMode: Image.PreserveAspectFit
            source: is_followed ? "qrc:/images/ic_heart.png" : "qrc:/images/ic_offer_heart.png"
            MouseArea{
                id: maDealFav
                anchors.fill: parent
                onClicked: {
                    if(app.menuIsShown)
                        menuView_back()
                    else{
                        if(userId === -1)
                            onLogin()
                        else
                        {
                            if(is_followed)
                            {
                                re.visible = false
                                re.height = 0
                            }
                            post("http://api.khuyenmai.re/api/v1/follow/deal/" + id)
                            is_followed = !is_followed
                            parent.source = is_followed ? "qrc:/images/ic_heart.png" : "qrc:/images/ic_offer_heart.png"
                        }
                    }
                }
            }

        }
        MouseArea {
            z: -1
            id: maOffer
            anchors.fill: parent
           // enabled: app.menuIsShown
            onPressed: {
                if(app.menuIsShown)
                    menuView_back()
            }

            onClicked: {
                if(app.menuIsShown)
                    menuView_back()
                else
                {
                    couponView.logoURL = app.prefixLogo(brand.logo)
                    //requestItem(listViewCoupon, "http://api.khuyenmai.re/api/v1/deal/?query=" + title)

                    couponView.queryCoupon = "http://api.khuyenmai.re/api/v1/deal/" + id + "/detail"
                    requestItem(listViewCoupon, couponView.queryCoupon)
                    onCoupon()
                }
            }
        }

    }    
}
