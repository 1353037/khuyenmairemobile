import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.0

Component
{
    Item
    {
        id: item_gift
        anchors.top: app.top
        anchors.right: app.right
        anchors.left: app.left
        height: app.height * 1.8
        width: app.width


        property int time: modelData.secondDiff
        Image
        {
            id: imgCoupon
            //anchors.verticalCenter: parent.verticalCenter
            //anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            //anchors.topMargin: parent.height * 0.1
            height: app.width * 0.5
            width: parent.width
            fillMode: Image.PreserveAspectCrop
            source: app.prefixLogo(modelData.thumb)
            smooth: true
            antialiasing: true
        }
        Rectangle
        {
            anchors.top: imgCoupon.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            color: palette.colorWhite
        }

        Label
        {
            id: labelCoupon
            anchors.top: imgCoupon.bottom
            anchors.left: parent.left
            anchors.right: parent.right

            property int time: parent.time
            Text
            {
                id: labelCouponTitle
                anchors.top: imgCoupon.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: 10 * app.dp
                text: modelData.name
                font.pixelSize: 30 * app.dp

                wrapMode: Text.WordWrap
                color: palette.colorTitle
            }

            Text
            {
                id: labelCouponHot
                anchors.top: labelCouponTitle.bottom
                anchors.left: parent.left
                anchors.topMargin: parent.height * 1.5
                anchors.leftMargin: parent.height
                enabled: true
                text: "CÒN: " + modelData.num_received
                color: palette.colorPrimary
                font.pixelSize: parent.height * 1.5
            }
            Text
            {
                id: labelCouponDate
                anchors.top: labelCouponTitle.bottom
                anchors.right: parent.right
                anchors.topMargin: parent.height * 1.9
                anchors.rightMargin: parent.height

                text: (parent.time < 0) ? "Hết hạn" : ((getDay(parent.time) >= 1) ? app.prefixDate(modelData.end_date) :
                                                   ((getHour(parent.time) >= 1) ? "Còn " + getHour(parent.time) + " giờ" :
                                                                                   ((getMinute(parent.time) >= 1) ? "Còn " + getMinute(parent.time) + " phút" :
                                                                                                                   "Còn " + getSecond(parent.time) + " giây")))
                color: palette.colorDetailDate
                font.pixelSize: parent.height
            }
            Timer {
                id: timer
                interval: 1000; repeat: true
                running: true
                triggeredOnStart: true
                onTriggered: {
                    parent.time = parseInt(parent.time) - 1
                    //if(parent.time <= 0)
                        //giftDetailView_back()
                }
            }
            Image
            {
                anchors.top: labelCouponTitle.bottom
                anchors.right: labelCouponDate.left
                anchors.topMargin: parent.height * 2
                anchors.rightMargin: parent.height * 0.5

                height: parent.height
                fillMode: Image.PreserveAspectFit
                //anchors.verticalCenter: parent.verticalCenter
                source: "qrc:/images/ic_offer_expiry.png"
            }
            Rectangle{
                anchors.top: labelCouponTitle.bottom
                anchors.topMargin: 10 * app.dp
                anchors.left: parent.left
                anchors.right: parent.right
                color: palette.color_bg
                height: 1
            }
            Rectangle{
                anchors.top: labelCouponDate.bottom
                anchors.topMargin: 15 * app.dp
                anchors.left: parent.left
                anchors.right: parent.right
                color: palette.color_bg
                height: 1
            }

            Label
            {
                id: labelCouponRate
                anchors.top: labelCouponHot.bottom
                anchors.topMargin: parent.height * 2
                anchors.left: parent.left
                anchors.right: parent.right

                Text
                {
                    anchors.top: parent.top
                    anchors.topMargin: -parent.height * 0.5
                    anchors.left: parent.left
                    anchors.leftMargin: parent.height * 0.5
                    text: "Hiện có: " +' <font color="#ff5400"><b>' + modelData.user_point + 'đ</b></font>'
                     font.pixelSize: 15 * app.dp
                }
                Text
                {
                    id: labelCouponRateText
                    anchors.top: parent.top
                    anchors.topMargin: parent.height * 0.8
                    anchors.left: parent.left
                    anchors.leftMargin: parent.height * 0.5
                    text: "Điểm cần: " +' <font color="#ff5400"><b>' + modelData.point + 'đ</b></font>'

                    font.pixelSize: 15 * app.dp
                }
                Button
                {
                    anchors.top: parent.top
                    anchors.topMargin: parent.height * -0.5
                    anchors.right: parent.right
                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            if(modelData.user_point < modelData.point || item_gift.time < 0)
                                return
                            if(!modelData.is_received)
                            {
                                    post("http://api.khuyenmai.re/api/v1/gift/" + modelData.id + "/receive")
                                    refreshGiftDetail()
                                    onRecOKGift()
                            }
                        }
                    }

                    Text
                    {
                        id: txtCouponReceived
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.pixelSize: 13 * app.dp
                        text: (item_gift.time < 0) ? "Hết hạn" : ((modelData.is_received) ? "Đã đổi" : (modelData.user_point < modelData.point) ? "Không đủ điểm" : "Đổi")
                        color: (item_gift.time) ? "red" : ((!modelData.is_received) ? ((modelData.user_point < modelData.point) ? "red" : palette.colorWhite) : palette.colorWhite)
                    }


                    style: ButtonStyle {
                            background: Rectangle {
                                implicitWidth: 100 * app.dp
                                implicitHeight: 50 * app.dp
                                color: (item_gift.time < 0) ? palette.black_40_transparent: ((!modelData.is_received) ? ((modelData.user_point < modelData.point) ? palette.black_40_transparent : palette.color_follow): palette.color_follow)
                                //border.width: control.activeFocus ? 2 : 1
                                //border.color: "#888"
                                radius: 80
                                /*
                                gradient: Gradient {
                                    GradientStop { position: 0 ; color: control.pressed ? "#ccc" : "#eee" }
                                    GradientStop { position: 1 ; color: control.pressed ? "#aaa" : "#ccc" }
                                }
                                */
                            }
                        }
                }

                Image{
                    id: recPromote
                    anchors.top: labelCouponRateText.bottom
                    anchors.topMargin: parent.height + 10 * app.dp
                    anchors.left: parent.left
                    anchors.right: parent.right
                    height: parent.height * 7
                    source: "qrc:/images/promote.jpg"
                    fillMode: Image.PreserveAspectCrop
                    smooth: true
                    antialiasing: true
                }
                TabView {
                    id: frame
                    anchors.top: recPromote.bottom
                    anchors.left: parent.left
                    anchors.right: parent.right
                    height: app.height
                    //anchors.bottom: parent.bottom
                    anchors.topMargin: 20

                    Tab {
                        title: string.benifit
                        Rectangle
                        {
                            anchors.fill: parent
                            Label
                            {
                                anchors.fill: parent
                                anchors.margins: app.dp * 20
                                text: modelData.description
                                wrapMode: Text.WordWrap
                            }
                        }

                    }
                    Tab { title: string.comment
                        Rectangle
                        {
                            anchors.fill: parent
                            color: palette.color_bg
                        }

                    }
                    style: TabViewStyle {
                        tabsAlignment: Qt.AlignHCenter
                            frameOverlap: 1
                            tab: Rectangle {
                                color: styleData.selected ? palette.colorPrimary : palette.colorWhite
                                border.color:  palette.colorPrimary
                                implicitWidth: (app.width - 50 * app.dp) / 2
                                implicitHeight: 40 * app.dp
                                radius: 1
                                Text {
                                    id: text
                                    anchors.centerIn: parent
                                    text: styleData.title
                                    color: styleData.selected ? palette.colorWhite : palette.colorPrimary
                                    font.pixelSize: app.dp * 20
                                }
                            }
                        }
                }
            }
        }

    }
}
