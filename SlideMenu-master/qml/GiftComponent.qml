import QtQuick 2.0
import QtQuick.Controls 1.2
//import QtQuick.Controls 2.1
import QtQuick.Controls.Styles 1.4
Component
{
    Rectangle
    {
        height: app.menuHeight
        width: parent.width
        color: maGift.pressed ? palette.color_bg : palette.colorWhite
        property int time: secondDiff
        Rectangle{
            anchors.bottom: parent.bottom
            color: palette.color_bg
            height: 1
            width: parent.width
        }
        MouseArea{
            id: maGift
            anchors.fill: parent
        }
        Text
        {
            anchors.margins: 10 * app.dp
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width * 0.5
           // height: parent.height
            text: name
            wrapMode: Text.WordWrap
        }
        Text
        {
            //anchors.margins: 10 * app.dp
            anchors.left: parent.left
            anchors.leftMargin: parent.width * 0.55
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width * 0.2
           // height: parent.height
            text: point
            wrapMode: Text.WordWrap
        }
        Label
        {
            //anchors.margins: 10 * app.dp
            anchors.right: parent.right
            anchors.rightMargin: 20 * app.dp
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter

            Image{
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                //anchors.horizontalCenter: parent.horizontalCenter
                //anchors.left: parent.left
                //anchors.leftMargin: 30 * app.dp
                width: 20 * app.dp
                height: 20 * app.dp
                source: "qrc:/images/more_detail_ic.png"
                fillMode: Image.PreserveAspectFit
                smooth: true
                antialiasing: true
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        giftDetailView.queryGiftDetail = "http://api.khuyenmai.re/api/v1/gift/" + id + "/detail"
                        requestItem(listViewGiftDetail, giftDetailView.queryGiftDetail)
                        onGiftDetail()
                    }
                }
            }
        }


    }

}
