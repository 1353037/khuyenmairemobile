import QtQuick 2.0
import QtQuick.Window 2.2
import Qt.labs.settings 1.0
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.0

Rectangle{
    id: dealsView
    width: parent.width
    height: parent.height
    x: - menuWidth
    y: 0
    z: 5
    Behavior on x { NumberAnimation { duration: app.durationOfMenuAnimation; easing.type: Easing.OutQuad } }

    color: palette.color_bg
    property string queryDeals: null
    Rectangle {
        id: dealsView_menuBar
        z: 5
        anchors.top: parent.top
        width: parent.width
        height: app.orientation == app.orientationLandscape ? 40*app.dp : menuHeight
        color: palette.colorPrimary
        Rectangle {
            id: dealsView_menuButton
            anchors.left: parent.left
            anchors.verticalCenter: parent.verticalCenter
            width: 1.2*height
            height: parent.height
            scale: dealsView_maMenuBar.pressed ? 1.2 : 1
            color: "transparent"
            Image {
                anchors.centerIn: parent
                height: parent.height * 0.5
                fillMode: Image.PreserveAspectFit
                source: "qrc:/images/ic_back.png"
            }
            MouseArea {
                id: dealsView_maMenuBar
                anchors.fill: parent
                onClicked: dealsView_backToMenu()
            }
            clip: true
        }
        Label {
            id: dealsView_titleText
            anchors.left: dealsView_menuButton.right
            anchors.rightMargin: 0.2 * parent.height
            anchors.verticalCenter: parent.verticalCenter
            text: string.app_name
            font.pixelSize: 0.35*parent.height
            color: palette.colorWhite
        }
    }

    Image {
        anchors.top: dealsView_menuBar.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        height: 6*app.dp
        z: 4
        source: "qrc:/images/shadow_title.png"
    }
    Loader
    {
        anchors.top: dealsView_menuBar.bottom
        anchors.topMargin: searchHeight * 2
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom

        ScrollView {
            id: scrollDealsView
         anchors.fill: parent
         flickableItem.interactive: true
         style: ScrollViewStyle {
                 transientScrollBars: true
                 handle: Item {
                     implicitWidth: 10
                     implicitHeight: 50
                     Rectangle {
                         color: palette.color_bg
                         anchors.fill: parent
                         anchors.topMargin: 6
                         anchors.leftMargin: 4
                         anchors.rightMargin: 4
                         anchors.bottomMargin: 6
                     }
                 }
                 scrollBarBackground: Item {
                     implicitWidth: 10
                     implicitHeight: 50
                 }
             }
            ListView
            {
                property string nextUrl: null
                property bool canAppend: true

                anchors.fill: parent

                id: listViewDeals
                delegate: offerComponent
                clip: true
                model: ListModel{}
                onContentYChanged:
                {
                    if(scrollDealsView.flickableItem.visibleArea.yPosition > 0.8){
                        if(nextUrl !== null)
                            request(listViewDeals, nextUrl)
                    }
                }
            }
            Component.onCompleted: {
                    dealsView.queryDeals = "http://api.khuyenmai.re/api/v1/deal"
                    request(listViewDeals, dealsView.queryDeals)
            }
        }

    }
}
