import QtQuick 2.0
import QtQuick.Controls 1.2
//import QtQuick.Controls 2.1
import QtQuick.Controls.Styles 1.4
Component
{
    Rectangle
    {
        id: re
        height: app.menuHeight
        width: parent.width
        color: maReceivedDeal.pressed ? palette.color_bg : palette.colorWhite
        property int time: secondDiff
        Rectangle{
            anchors.bottom: parent.bottom
            color: palette.color_bg
            height: 1
            width: parent.width
        }
        MouseArea{
            id: maReceivedDeal
            anchors.fill: parent
        }

        Image{
            anchors.left: parent.left
            anchors.margins: 10 * app.dp
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width * 0.3
            height: parent.height
            source: app.prefixLogo(brand.logo)
            fillMode: Image.PreserveAspectFit
            smooth: true
            antialiasing: true
        }
        Label{
            id: lbCouponCode
            anchors.left: parent.left
            anchors.leftMargin: parent.width * 0.35
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width * 0.3
            property int time: parent.time
            Text{
                anchors.fill: parent
                color: (parent.time < 0) ? "red" : "green"
                text:  (parent.time > 0) ? getHour(parent.time) + "h" + getMinute(parent.time) + "m" + getSecond(parent.time) + "s": "Hết hạn"
           }
        }
        Timer {
            id: timer
            interval: 1000; repeat: true
            running: true
            triggeredOnStart: true
            onTriggered: {
                parent.time = parseInt(parent.time) - 1
            }
        }
        Label{
            id: lbPoint
            anchors.left: parent.left
            anchors.leftMargin: parent.width * 0.6
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: Text.AlignHCenter
            width: parent.width * 0.2
            Text{
                anchors.fill: parent
                color: (parent.time < 0) ? "red" : "green"
                text: "+" + points + "đ"
            }

        }
        Image{
            anchors.right: parent.right
            anchors.rightMargin: 40 * app.dp
            anchors.verticalCenter: parent.verticalCenter
            width: 20 * app.dp
            height: 20 * app.dp
            source: "qrc:/images/more_detail_ic.png"
            fillMode: Image.PreserveAspectFit
            smooth: true
            antialiasing: true
            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if(recYN.y > app.height)
                    {
                        couponView.prevView = "received"
                        couponView.logoURL = app.prefixLogo(brand.logo)
                        couponView.queryCoupon = "http://api.khuyenmai.re/api/v1/deal/" + deal_id + "/detail"
                        requestItem(listViewCoupon, couponView.queryCoupon)
                        onCoupon()
                    }
                }
            }
        }
        Image{
            anchors.right: parent.right
            anchors.rightMargin: 10 * app.dp
            anchors.verticalCenter: parent.verticalCenter
            width: 20 * app.dp
            height: 20 * app.dp
            source: "qrc:/images/delete_ic.png"
            fillMode: Image.PreserveAspectFit
            smooth: true
            antialiasing: true

            MouseArea{
                anchors.fill: parent
                onClicked: {
                    if(recYN.y > app.height)
                    {
                         receivedDealView.deleteURL = "http://api.khuyenmai.re/api/v1/deal/" + id + "/" + coupon_id + "/deleted"
                         recYN.y = app.width / 2.5                         
                    }
                }
            }
        }


/*
        Image{
            anchors.left: parent.left
            anchors.leftMargin: parent.width / 0.3
            anchors.verticalCenter: parent.verticalCenter
            width: parent.width / 0.2
            source: brand_logo
            fillMode: Image.PreserveAspectFit
            smooth: true
            antialiasing: true
        }*/
    }

}
