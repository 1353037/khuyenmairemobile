import QtQuick 2.4
import QtQuick.Controls 1.2


Item {
    id: menu

    signal menuItemClicked( string page )
    property alias currentItem: listViewMenu.currentIndex

    ListModel {
        id: modelMenu
        ListElement {
            item: "page_brands"
            icon: "qrc:/images/icon_game.png"
            page: "brands"
        }
        ListElement {
            item: "settings"
            icon: "qrc:/images/icon_settings.png"
            page: "PageSettings.qml"
        }
        ListElement {
            item: "about"
            icon: "qrc:/images/icon_info.png"
            page: "about"
        }
    }

    function textItemMenu( item )
    {
        var textReturn = ""
        switch( item ) {
        case "page_brands":
            textReturn = qsTr("D.sách thương hiệu")
            break;
        case "settings":
            textReturn = qsTr("Cài đặt")
            break;
        case "about":
            textReturn = qsTr("Thông tin")
            break;
        case "log":
            textReturn = "Log"
            break;
        }
        return textReturn
    }

    Rectangle {
        id: logoWtapper
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        width: parent.width
        height: width*0.5
        color: palette.primary //"#3078fe" //this color is equal to the background of imgLogo
        clip: true
        Image {
            id: imgLogo
            source: "qrc:/images/ph_logo.png"
            //height: parent.height
            //width: parent.width
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            height: parent.height * 0.8
            fillMode: Image.PreserveAspectFit
            antialiasing: true
            smooth: true
            //anchors.top: parent.top
            //anchors.left: parent.left
            //opacity: 0.5
        }
    }
    Image {
        id: imgShadow
        anchors.top: logoWtapper.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        height: 10*app.dp
        z: 4
        source: "qrc:/images/shadow_title.png"
    }
    ScrollView {
        anchors.top: logoWtapper.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
     flickableItem.interactive: false
        ListView {
            id: listViewMenu
            anchors.fill: parent
            clip: true
            model: modelMenu
            delegate: componentDelegate
        }
    }

    Component {
        id: componentDelegate

        Rectangle {
            id: wrapperItem
            height: 60*app.dp
            width: parent.width
            color: ma.pressed ? palette.currentHighlightItem : "transparent"
            Image {
                id: imgItem
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.leftMargin: 0.5*imgItem.width
                height: parent.height*0.5
                width: height
                source: icon
                visible: icon != ""
                smooth: true
                antialiasing: true
            }

            Label {
                id: textItem
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: imgItem.right
                anchors.leftMargin: 0.7*imgItem.width
                text: textItemMenu( item )
                font.pixelSize: parent.height*0.3
                color: wrapperItem.ListView.isCurrentItem ? palette.darkPrimary : palette.primaryText
            }


            MouseArea {
                id: ma
                anchors.fill: parent
                enabled: app.menuIsShown
                onPressed: listViewMenu.currentIndex = index

                onClicked: menu.menuItemClicked( page )

            }
        }

    }
}

