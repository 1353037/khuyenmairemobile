import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.0

Component
{
    Item
    {
        anchors.top: app.top
        anchors.right: app.right
        anchors.left: app.left
        height: app.height * 1.8
        width: app.width


        property int time: modelData.secondDiff
        Image
        {
            id: imgCoupon
            //anchors.verticalCenter: parent.verticalCenter
            //anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            //anchors.topMargin: parent.height * 0.1
            height: app.width * 0.5
            width: parent.width
            fillMode: Image.PreserveAspectCrop
            source: app.prefixLogo(modelData.img_url)
            smooth: true
            antialiasing: true
        }
        Rectangle
        {
            anchors.top: imgCoupon.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            color: palette.colorWhite
        }

        Label
        {
            id: labelCoupon
            anchors.top: imgCoupon.bottom
            anchors.left: parent.left
            anchors.right: parent.right

            property int time: parent.time
            Text
            {
                id: labelCouponTitle
                anchors.top: imgCoupon.bottom
                anchors.left: parent.left
                anchors.right: parent.right
                anchors.leftMargin: 10 * app.dp
                text: modelData.title
                font.pixelSize: 30 * app.dp

                wrapMode: Text.WordWrap
                color: palette.colorTitle
            }

            Text
            {
                id: labelCouponHot
                anchors.top: labelCouponTitle.bottom
                anchors.left: parent.left
                anchors.topMargin: parent.height * 1.2
                anchors.leftMargin: 10 * app.dp
                enabled: true
                text: modelData.is_highlight ? modelData.highlight : labelCouponHot.enabled = false
                color: palette.colorPrimary
                font.pixelSize: 25 * app.dp
            }
            Text
            {
                id: labelCouponDate
                anchors.top: labelCouponTitle.bottom
                anchors.right: parent.right
                anchors.topMargin: parent.height * 1.5
                anchors.rightMargin: 10 * app.dp

                text: parent.time < 0 ? "Hết hạn" : (getDay(parent.time) >= 1) ? app.prefixDate(modelData.end_date) :
                                                   ((getHour(parent.time) >= 1) ? "Còn " + getHour(parent.time) + " giờ" :
                                                                                   ((getMinute(parent.time) >= 1) ? "Còn " + getMinute(parent.time) + " phút" :
                                                                                                                   "Còn " + getSecond(parent.time) + " giây"))
                color: parent.time < 0 ? "red" : palette.colorDetailDate
                font.pixelSize: 15 * app.dp
            }
            Timer {
                id: timer
                interval: 1000; repeat: true
                running: true
                triggeredOnStart: true
                onTriggered: {
                    parent.time = parseInt(parent.time) - 1
                    if(parent.time === 0)
                        couponView_back()
                }
            }
            Image
            {
                anchors.top: labelCouponTitle.bottom
                anchors.right: labelCouponDate.left
                anchors.topMargin: parent.height * 1.5
                anchors.rightMargin: 5 * app.dp

                height: 15 * app.dp
                fillMode: Image.PreserveAspectFit
                //anchors.verticalCenter: parent.verticalCenter
                source: "qrc:/images/ic_offer_expiry.png"
            }
            Rectangle{
                anchors.top: labelCouponTitle.bottom
                anchors.topMargin: 10 * app.dp
                anchors.left: parent.left
                anchors.right: parent.right
                color: palette.color_bg
                height: 1
            }
            Rectangle{
                anchors.top: labelCouponDate.bottom
                anchors.topMargin: 15 * app.dp
                anchors.left: parent.left
                anchors.right: parent.right
                color: palette.color_bg
                height: 1
            }

            Label
            {
                id: labelCouponRate
                anchors.top: labelCouponHot.bottom
                anchors.topMargin: parent.height * 2
                anchors.left: parent.left
                anchors.right: parent.right

                Text
                {
                    id: labelCouponRateText
                    anchors.top: parent.top
                    anchors.topMargin: parent.height * 0.3
                    anchors.left: parent.left
                    anchors.leftMargin: parent.height * 0.3
                    text: string.rate +' <font color="#ff5400"><b>' + modelData.rate_deal + '</b></font>'

                    font.pixelSize: 13 * app.dp
                }
                Label
                {
                    id: labelRate
                    anchors.top: parent.top
                    anchors.topMargin: 5 * app.dp
                    anchors.left: labelCouponRateText.right
                    Image
                    {
                        id: star1
                        anchors.left: parent.left
                        anchors.leftMargin: 5 * app.dp
                        height: 20 * app.dp
                        width: 20 * app.dp
                       // fillMode: Image.PreserveAspectFit
                        anchors.verticalCenter: parent.verticalCenter
                        source: "qrc:/images/ic_offer_favorite.png"
                    }
                    Image
                    {
                        id: star2
                        anchors.left: star1.right
                        anchors.leftMargin: 5 * app.dp
                        height: 20 * app.dp
                        width: 20 * app.dp
                        fillMode: Image.PreserveAspectFit
                        anchors.verticalCenter: parent.verticalCenter
                        source: "qrc:/images/ic_offer_favorite.png"
                    }
                    Image
                    {
                        id: star3
                        anchors.left: star2.right
                        anchors.leftMargin: 5 * app.dp
                        height: 20 * app.dp
                        width: 20 * app.dp
                        fillMode: Image.PreserveAspectFit
                        anchors.verticalCenter: parent.verticalCenter
                        source: "qrc:/images/ic_offer_favorite.png"
                    }
                    Image
                    {
                        id: star4
                        anchors.left: star3.right
                        anchors.leftMargin: 5 * app.dp
                        height: 20 * app.dp
                        width: 20 * app.dp
                        fillMode: Image.PreserveAspectFit
                        anchors.verticalCenter: parent.verticalCenter
                        source: "qrc:/images/ic_offer_favorite.png"
                    }
                    Image
                    {
                        id: star5
                        anchors.left: star4.right
                        anchors.leftMargin: 5 * app.dp
                        height: 20 * app.dp
                        width: 20 * app.dp
                        fillMode: Image.PreserveAspectFit
                        anchors.verticalCenter: parent.verticalCenter
                        source: "qrc:/images/ic_offer_favorite.png"
                    }
                }
                Button
                {
                    anchors.top: parent.top
                    anchors.topMargin: parent.height * -0.5
                    anchors.right: parent.right
                    //anchors.verticalCenter: parent.verticalCenter
                    //anchors.horizontalCenter: parent.horizontalCenter
                    //color: third
                    //radius: 8
                    MouseArea{
                        anchors.fill: parent
                        onClicked: {
                            if(modelData.is_used || modelData.is_exprired || !modelData.can_receive || !modelData.is_start)
                                  return
                            if(userId === -1){
                                loginView.prevView = "coupon"
                                onLogin()
                            }
                            else
                            {
                                if(!modelData.is_received)
                                {
                                        post("http://api.khuyenmai.re/api/v1/deal/" + modelData.id + "/received")
                                       // modelData.is_received = 1
                                       // txtCouponReceived.text = "Xem ưu đãi"
                                       // requestItem(listViewReceivedDealSuccess, "http://api.khuyenmai.re/api/v1/deal/")
                                    refreshCoupon()

                                }
                                else
                                {
                                requestItem(listViewReceivedDealSuccess, "http://api.khuyenmai.re/api/v1/deal/" + modelData.id + "/show")
                                onReceiveCouponSuccess()
                                }
                            }
                        }
                    }

                    Text
                    {
                        id: txtCouponReceived
                        anchors.verticalCenter: parent.verticalCenter
                        anchors.horizontalCenter: parent.horizontalCenter
                        font.pixelSize: 15 * app.dp
                        text: (modelData.is_used) ? "Đã dùng" : ((!modelData.is_start) ? "Chưa bắt đầu" : (modelData.is_exprired || !modelData.can_receive) ? "Kết thúc" : modelData.is_received ? "Xem ưu đãi" : "Nhận ưu đãi")
                        color:(modelData.is_exprired || !modelData.can_receive) ? "red" : palette.colorWhite
                    }


                    style: ButtonStyle {
                            background: Rectangle {
                                implicitWidth: 100 * app.dp
                                implicitHeight: 50 * app.dp
                                color: (modelData.is_exprired || !modelData.can_receive) ? palette.black_40_transparent : palette.color_follow
                                //border.width: control.activeFocus ? 2 : 1
                                //border.color: "#888"
                                radius: 80
                                /*
                                gradient: Gradient {
                                    GradientStop { position: 0 ; color: control.pressed ? "#ccc" : "#eee" }
                                    GradientStop { position: 1 ; color: control.pressed ? "#aaa" : "#ccc" }
                                }
                                */
                            }
                        }
                }

                Image{
                    id: recPromote
                    anchors.top: labelCouponRateText.bottom
                    anchors.topMargin: parent.height + 10 * app.dp
                    anchors.left: parent.left
                    anchors.right: parent.right
                    height: parent.height * 7
                    source: "qrc:/images/promote.jpg"
                    fillMode: Image.PreserveAspectCrop
                    smooth: true
                    antialiasing: true
                }
                TabView {
                    id: frame
                    anchors.top: recPromote.bottom
                    anchors.left: parent.left
                    anchors.right: parent.right
                    height: app.height
                    //anchors.bottom: parent.bottom
                    anchors.topMargin: 20

                    Tab {
                        title: string.benifit
                        Rectangle
                        {
                            anchors.fill: parent
                            Label
                            {
                                anchors.fill: parent
                                anchors.margins: app.dp * 20
                                text: modelData.condition
                                wrapMode: Text.WordWrap
                                font.pixelSize: 15 * app.dp
                            }
                        }

                    }
                    Tab {
                        title: string.store_location
                        Rectangle
                        {
                            color: "black"
                            anchors.fill: parent
                            ScrollView
                            {
                                id: scrollLocation
                                anchors.fill: parent
                                flickableItem.interactive: true

                                style: ScrollViewStyle {
                                        transientScrollBars: true
                                        handle: Item {
                                            implicitWidth: 30
                                            implicitHeight: 200
                                            Rectangle {
                                                color: palette.color_bg
                                                anchors.fill: parent
                                                anchors.topMargin: 6
                                                anchors.leftMargin: 4
                                                anchors.rightMargin: 4
                                                anchors.bottomMargin: 6
                                            }
                                        }
                                        scrollBarBackground: Item {
                                            implicitWidth: 10
                                            implicitHeight: 200
                                        }
                                    }
                                ListView{
                                    id: listViewLocation
                                    property string nextUrl: ""
                                    property bool canAppend: true
                                    spacing: 1
                                    model: ListModel{}
                                    delegate:
                                        Rectangle
                                        {
                                            width: parent.width
                                            height: 40 * app.dp
                                            color: maCouponAddressRec.pressed ? palette.color_bg : palette.colorWhite
                                            Rectangle{
                                                anchors.bottom: parent.bottom
                                                width: parent.width
                                                height: 1
                                                color: palette.color_bg
                                            }
                                            MouseArea
                                            {
                                                id: maCouponAddressRec
                                                anchors.fill: parent
                                            }

                                            Label
                                            {
                                                anchors.top: parent.top
                                                anchors.left: parent.left
                                                anchors.right: parent.right
                                                anchors.leftMargin: 15 * app.dp
                                                anchors.topMargin: 5 * app.dp
                                                Image
                                                {
                                                    id: icAddress
                                                    anchors.left: parent.left
                                                    fillMode: Image.PreserveAspectFit
                                                    source: "qrc:/images/ic_offer_navigation.png"
                                                    smooth: true
                                                    antialiasing: true
                                                    height: 15 * app.dp
                                                }

                                                Text
                                                {
                                                    anchors.left: icAddress.right
                                                    text: prefixAddress(address)
                                                    font.pixelSize: 13 * app.dp
                                                }
                                            }
                                            Label
                                            {
                                                anchors.bottom: parent.bottom
                                                anchors.left: parent.left
                                                anchors.right: parent.right
                                                anchors.leftMargin: 20 * app.dp
                                                Image
                                                {
                                                    id: icPhoneNum
                                                    width: 13 * app.dp

                                                    fillMode: Image.PreserveAspectFit
                                                    source: "qrc:/images/ic_offer_phone.png"
                                                    smooth: true
                                                    antialiasing: true
                                                    visible: (phone_number === "") ? false : true
                                                }
                                                Text
                                                {
                                                    anchors.left: icPhoneNum.right
                                                    text: phone_number
                                                    font.pixelSize: 10 * app.dp
                                                }
                                            }
                                            Image{
                                                anchors.right: parent.right
                                                anchors.rightMargin: 20 * app.dp
                                                anchors.verticalCenter: parent.verticalCenter
                                                height: 20 * app.dp
                                                width: height
                                                scale: maCouponAddress.pressed ? 1.2 : 1
                                                fillMode: Image.PreserveAspectFit
                                                source: "qrc:/images/ic_more.png"

                                                MouseArea {
                                                    id: maCouponAddress
                                                    anchors.fill: parent
                                                    onClicked: {
                                                        app.latitude = latitude
                                                        app.longitude = longitude
                                                        //requestItem(listViewBrand, "api/v1/brand/?query=" + brand_name)
                                                        app.address = address
                                                        locationView.prevView = "coupon"
                                                        onLocation()
                                                    }
                                                }
                                            }
                                        }

                                    clip: true
                                    onContentYChanged:
                                    {
                                        if(scrollLocation.flickableItem.visibleArea.yPosition > 0.8){
                                            if(nextUrl !== null)
                                                request(listViewLocation, nextUrl)
                                        }
                                    }
                                    property alias isEmpty: listViewLocation_error.opa
                                    ErrorComponent
                                    {
                                        id: listViewLocation_error
                                        property string text: string.empty_list_store
                                    }

                                    Component.onCompleted:
                                    {
                                        request(listViewLocation, app.rootURL + "api/v1/brand/" + modelData.id +"/detail/store")
                                    }
                                }
                            }
                        }


                    }
                    Tab { title: string.comment
                        Rectangle
                        {
                            anchors.fill: parent
                            color: palette.color_bg
                        }

                    }
                    style: TabViewStyle {
                        tabsAlignment: Qt.AlignHCenter
                            frameOverlap: 1
                            tab: Rectangle {
                                color: styleData.selected ? palette.colorPrimary : palette.colorWhite
                                border.color:  palette.colorPrimary
                                implicitWidth: (app.width - 50 * app.dp) / 3
                                implicitHeight: 40 * app.dp
                                radius: 1
                                Text {
                                    id: text
                                    anchors.centerIn: parent
                                    text: styleData.title
                                    color: styleData.selected ? palette.colorWhite : palette.colorPrimary
                                    font.pixelSize: app.dp * 20
                                }
                            }
                        }
                }
            }            
        }

    }
}
