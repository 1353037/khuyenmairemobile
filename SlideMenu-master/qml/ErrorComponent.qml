import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.0
Rectangle
{
    z: -1
    anchors.fill: parent
    color: palette.colorWhite
    property alias opa: textError.opacity
    Text
    {
        id: textError
        anchors.top: parent.top
        anchors.topMargin: app.dp * 30
        anchors.horizontalCenter: parent.horizontalCenter
        text: parent.text//string.empty_list
        font.pixelSize: 15 * app.dp
        opacity: 0
    }
}
