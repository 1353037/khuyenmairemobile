import QtQuick 2.0
import QtQuick.Controls 1.2
import QtQuick.Controls.Styles 1.0

Component
{
    Item
    {
        anchors.fill: parent
        Rectangle
        {
            id: recInfo
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.right: parent.right
            height: 200 * app.dp
            MouseArea
            {
                anchors.fill: parent
                onClicked: {}
            }
            //width:
            color: palette.colorWhite
            Button{
                id: btnLogout
                anchors.top: parent.top
                anchors.right: parent.right
                anchors.margins: 20 * app.dp
                text: string.log_out

                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: 80 * app.dp
                            implicitHeight: 30 * app.dp
                            color: palette.color_bg
                            radius: 10
                            border.width: 1
                        }
                    }
                MouseArea{
                    anchors.fill: parent
                    onClicked: {
                        onLogout()
                    }
                }
            }
            Image
            {
                id: imgProfile
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                //anchors.top: parent.top
                anchors.topMargin: 10 * app.dp
                height: 80 * app.dp
                width: height
                fillMode: Image.PreserveAspectFit
                source: modelData.img_url !== "" ? app.prefixLogo(modelData.img_url) : "qrc:/images/default_avatar.png"
                //source: "https://storage.googleapis.com/jamja-prod/gcs_full_5786019e6e0b49709e59cea3-2016-07-13-085353.jpg" //image
                smooth: true
                antialiasing: true
            }

            Button{
                id: btnTotal
                anchors.top: imgProfile.bottom
                anchors.left: parent.left
                anchors.topMargin:  20 * app.dp
                anchors.leftMargin: 50 * app.dp
                text:  string.total_points + ' <font color="#ff5400"><b>' + modelData.total_points + '</b></font>'

                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: 100 * app.dp
                            implicitHeight: 30 * app.dp
                            color: palette.colorWhite
                            radius: 10
                            border.width: 1
                        }
                    }
            }
            Button{
                anchors.top: imgProfile.bottom
                anchors.right: parent.right
                anchors.topMargin: 20 * app.dp
                anchors.rightMargin: 50 * app.dp
                //anchors.verticalCenter: parent.verticalCenter
                //anchors.ho: parent.ho
                text:  string.current_points + ' <font color="#ff5400"><b>' + modelData.current_points + 'đ</b></font>'

                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: 100 * app.dp
                            implicitHeight: 30 * app.dp
                            color: palette.colorWhite
                            //border.width: control.activeFocus ? 2 : 1
                            //border.color: "#888"
                            radius: 10
                            border.width: 1
                        }
                    }
            }
        }
        Rectangle{
            id: menuProfile
            anchors.top:recInfo.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 50 * app.dp
            color: mamenuProfile.pressed ? palette.color_bg : palette.colorWhite
            Rectangle{
                anchors.bottom: parent.bottom
                width: parent.width
                height: 1
                color: palette.color_bg
            }
            Rectangle{
                anchors.top: parent.top
                width: parent.width
                height: 1
                color: palette.color_bg
            }
            Label{
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Thông tin cá nhân"
                font.pixelSize: 15 * app.dp
            }
            MouseArea{
                id: mamenuProfile
                anchors.fill: parent
                onClicked: {
                    if(app.menuIsShown)
                        menuView_back()
                    else
                    {                        
                        request(listViewInfoUser, "http://api.khuyenmai.re/api/v1/profile");
                        onInfoUser()
                    }
                }
            }
        }
        Rectangle{
            id: menuReceived
            anchors.top:menuProfile.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 50 * app.dp
            color: mamenuReceived.pressed ? palette.color_bg : palette.colorWhite
            Rectangle{
                anchors.bottom: parent.bottom
                width: parent.width
                height: 1
                color: palette.color_bg
            }
            Label{
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Ưu đãi đã nhận"
                font.pixelSize: 15 * app.dp
            }

            MouseArea{
                id: mamenuReceived
                anchors.fill: parent
                onClicked: {
                    if(app.menuIsShown)
                        menuView_back()
                    else
                    {
                        resetListView(listViewReceivedDeal)
                        request(listViewReceivedDeal, "http://api.khuyenmai.re/api/v1/deal/received");
                        onReceivedDeal()
                    }
                }
                onPressed: {
                               if(app.menuIsShown)
                                   menuView_back()
                           }
            }
        }
        Rectangle{
            id: menuHistory
            anchors.top:menuReceived.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 50 * app.dp
            color: mamenuHistory.pressed ? palette.color_bg : palette.colorWhite
            Rectangle{
                anchors.bottom: parent.bottom
                width: parent.width
                height: 1
                color: palette.color_bg
            }
            Label{
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Ưu đãi đã sử dụng"
                font.pixelSize: 15 * app.dp
            }
            MouseArea{
                id: mamenuHistory
                anchors.fill: parent
                onPressed: {
                               if(app.menuIsShown)
                                   menuView_back()
                           }
                onClicked: {
                    if(app.menuIsShown)
                        menuView_back()
                    else
                    {
                        listViewReceivedDealHistory.model.clear()
                        request(listViewReceivedDealHistory, "http://api.khuyenmai.re/api/v1/deal/receivedHistory");
                        onReceivedDealHistory()
                    }
                }
            }
        }
        Rectangle{
            id: menuGift
            anchors.top:menuHistory.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 50 * app.dp
            color: mamenuGift.pressed ? palette.color_bg : palette.colorWhite
            Rectangle{
                anchors.bottom: parent.bottom
                width: parent.width
                height: 1
                color: palette.color_bg
            }
            Label{
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                text: "Đổi quà"
                font.pixelSize: 15 * app.dp
            }
            MouseArea{
                id: mamenuGift
                anchors.fill: parent
                onClicked: {
                    if(app.menuIsShown)
                        menuView_back()
                    else
                    {
                        request(listViewGift, "http://api.khuyenmai.re/api/v1/gift");
                        onGift()
                    }
                }
            }
        }
        Rectangle{
            anchors.top:menuGift.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            height: 300 * app.dp
            MouseArea{
                anchors.fill: parent
                onClicked: {}
            }
        }
    }
}
