import QtQuick 2.0

Component
{
    Rectangle
    {
        anchors.left: parent.left
        anchors.right: parent.right
        //property string text:  '<b>' + brand + '</b>' + " vừa cập nhật ưu đãi mới. "
        //                       + '"<font color="#ff5400"><b>' + title + '</b></font>"'
        //                       + " đến " + date_expire
        height: 100 * app.dp
        color: !is_view ? palette.colorWhite : palette.colorPrimary

        MouseArea{
            anchors.fill: parent
            onClicked:{
                if(app.menuIsShown)
                    menuView_back()
                else
                {
                    couponView.logoURL = app.prefixLogo(deal.brand.logo)
                    requestItem(listViewCoupon, "http://api.khuyenmai.re/api/v1/deal/")
                    requestItem(listViewCoupon, "http://api.khuyenmai.re/api/v1/deal/" + deal.title + "/detail")
                    onCoupon()
                }
            }

        }

        Image
        {
            id: imgLogo
            anchors.verticalCenter: parent.verticalCenter
            anchors.left: parent.left
            anchors.leftMargin: parent.height*0.1
            height: parent.height * 0.9
            width: height
            fillMode: Image.PreserveAspectCrop
            source: deal.thumb
            smooth: true
            antialiasing: true
        }

        Text
        {
            id: notiBrandText
            anchors.left: imgLogo.right
            anchors.leftMargin: parent.height * 0.1
            anchors.top: parent.top
            anchors.topMargin: parent.height * 0.1
            anchors.bottom: imgClock.top
            anchors.right: parent.right
            text: '<b>' + deal.brand.brand_name + '</b>' + " vừa cập nhật ưu đãi mới. "
                  + '"<font color="#ff5400"><b>' + deal.title + '</b></font>"'
                  + " đến " + app.prefixDate(deal.end_date)

            wrapMode: Text.WordWrap
           // clip: true
        }
        Image
        {
            id: imgClock
            anchors.bottom: parent.bottom
            anchors.bottomMargin: parent.height * 0.1
            anchors.left: imgLogo.right
            anchors.leftMargin: parent.height * 0.1
            width: parent.height * 0.1
            height: width
            fillMode: Image.PreserveAspectFit
            source: "qrc:/images/ic_offer_date.png"
        }
        Text
        {
            anchors.left: imgClock.right
            anchors.leftMargin: 5* app.dp
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10 * app.dp
            font.pixelSize: 10 * app.dp
            text: app.prefixDate(show_date)
            color: "black"
        }
    }
}
