import QtQuick 2.0
import QtQuick.Controls 1.2
//import QtQuick.Controls 2.1
import QtQuick.Controls.Styles 1.4
Component
{
    Rectangle
    {
        id: rootInfo
        anchors.fill: parent

        property bool readOnly: true
        Label{
            id: lbImage
            anchors.fill: parent
            width: app.width / 3
            Image {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.top: parent.top
                anchors.topMargin: 20 * app.dp
                height:  app.width / 3
                source: "qrc:/images/default_avatar.png"
                fillMode: Image.PreserveAspectFit
            }
        }
        Label
        {
            id: lbInfo
            anchors.left: parent.left
            anchors.top: parent.top
            anchors.topMargin: app.width / 3 + 20 * app.dp
            //anchors.bottom: parent.bottom
            anchors.right: parent.right
            anchors.margins: 30 * app.dp
            height: 210 * app.dp

            Label{
                id: subName
                anchors.top: parent.top
                anchors.left: parent.left
                anchors.right: parent.right
                //anchors.horizontalCenter: parent.horizontalCenter

                Text
                {
                    id: textName
                    anchors.left: parent.left
                    anchors.top: parent.top
                    width: 60 * app.dp
                    text: "Họ tên: "
                }
                TextField
                {
                    id: tfsubName
                    //anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.leftMargin: 60 * app.dp
                    anchors.right: parent.right
                    readOnly: rootInfo.readOnly
                    //placeholderText:
                    text: name
                    font.pixelSize: 15 * app.dp
                    horizontalAlignment: TextInput.AlignHCenter
                    style: TextFieldStyle {
                            textColor: palette.colorPrimary
                            background: Rectangle {
                                radius: 5
                                //implicitWidth: 100
                                //implicitHeight: 24
                                border.color: rootInfo.readOnly ? palette.colorWhite : palette.color_bg
                                border.width: 5
                            }
                        }
                }
            }
            Label{
                id: subEmail
                anchors.top: subName.bottom
                anchors.topMargin: 20 * app.dp
                anchors.left: parent.left
                anchors.right: parent.right
                //anchors.horizontalCenter: parent.horizontalCenter

                Text
                {
                    id: textEmail
                    anchors.left: parent.left
                    anchors.top: parent.top
                    width: 60 * app.dp
                    text: "Email: "
                }
                TextField
                {
                    id: tfsubEmail
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.leftMargin: 60 * app.dp
                    anchors.right: parent.right
                    readOnly: rootInfo.readOnly
                    //placeholderText:
                    text: email
                    //width: text
                    font.pixelSize: 15 * app.dp
                    horizontalAlignment: TextInput.AlignHCenter
                    style: TextFieldStyle {
                            textColor: palette.colorPrimary
                            background: Rectangle {
                                radius: 5
                                //implicitWidth: 100
                                //implicitHeight: 24
                                border.color: rootInfo.readOnly ? palette.colorWhite : palette.color_bg
                                border.width: 5
                            }
                        }
                }
            }
            Label{
                id: subPhone
                anchors.top: subEmail.bottom
                anchors.topMargin: 20 * app.dp
                anchors.left: parent.left
                anchors.right: parent.right
                //anchors.horizontalCenter: parent.horizontalCenter

                Text
                {
                    id: textPhone
                    anchors.left: parent.left
                    anchors.top: parent.top
                    width: 60 * app.dp
                    text: "SĐT: "
                }
                TextField
                {
                    //anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.leftMargin: 60 * app.dp
                    anchors.right: parent.right
                    readOnly: true
                    //placeholderText:
                    text: phone
                    width: TextField.text
                    font.pixelSize: 15 * app.dp
                    horizontalAlignment: TextInput.AlignHCenter
                    style: TextFieldStyle {
                            textColor: palette.colorPrimary
                            background: Rectangle {
                                radius: 5
                                //implicitWidth: 100
                                //implicitHeight: 24
                                border.color: palette.colorWhite
                                border.width: 5
                            }
                        }
                }
            }
            Label{
                id: subDOB
                anchors.top: subPhone.bottom
                anchors.topMargin: 20 * app.dp
                anchors.left: parent.left
                anchors.right: parent.right
                //anchors.horizontalCenter: parent.horizontalCenter

                Text
                {
                    id: textDOB
                    anchors.left: parent.left
                    anchors.top: parent.top
                    width: 60 * app.dp
                    text: "Ngày sinh (dd-mm-yyyy): "
                }
                TextField
                {
                    id: tfsubDOB
                    //anchors.horizontalCenter: parent.horizontalCenter
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.leftMargin: 160 * app.dp
                    anchors.right: parent.right
                    readOnly: rootInfo.readOnly
                    //placeholderText:
                    text: birthday === "" ?  "" : app.prefixDate(birthday)
                    //width: text
                    font.pixelSize: 15 * app.dp
                    horizontalAlignment: TextInput.AlignHCenter
                    style: TextFieldStyle {
                            textColor: palette.colorPrimary
                            background: Rectangle {
                                radius: 5
                                //implicitWidth: 100
                                //implicitHeight: 24
                                border.color: rootInfo.readOnly ? palette.colorWhite : palette.color_bg
                                border.width: 5
                            }
                        }
                }
            }
            Label{
                id: subSex
                anchors.top: subDOB.bottom
                anchors.topMargin: 20 * app.dp
                anchors.left: parent.left
                anchors.right: parent.right
                //anchors.horizontalCenter: parent.horizontalCenter

                Text
                {
                    id: textSex
                    anchors.left: parent.left
                    anchors.top: parent.top
                    anchors.topMargin: 10 * app.dp
                    width: 60 * app.dp
                    text: "Giới tính: "
                }
                Label{
                    enabled: (txtCouponReceived.text !== "Chỉnh sửa")
                    anchors.top: parent.top
                    anchors.left: parent.left
                    anchors.leftMargin: 60 * app.dp
                    anchors.right: parent.right
                    ExclusiveGroup { id: tabPositionGroup

                    }
                    RadioButton {
                        id: rdMale
                        anchors.top: parent.top
                        text: "Nam"                       
                        checked: is_male ? 1 : 0
                        exclusiveGroup: tabPositionGroup
                    }
                    RadioButton {
                        anchors.top: parent.top
                        anchors.topMargin: 30 * app.dp
                        text: "Nữ"
                        checked: !is_male ? 1 : 0
                        exclusiveGroup: tabPositionGroup
                    }
                }

            }
        }
        Label
        {
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.leftMargin: 30 * app.dp
            anchors.top: lbInfo.bottom
            Text{
                id: infoError
                color: palette.colorPrimary
            }
        }

        Label{
            anchors.left: parent.left
            anchors.right: parent.right

            anchors.top: lbInfo.bottom
            anchors.topMargin: 50 * app.dp
            height: 50 * app.dp
            Button
            {
                anchors.rightMargin: 30 * app.dp
                anchors.right: parent.right
                anchors.top: parent.top
                height: parent.height
                MouseArea{
                    anchors.fill: parent
                    onClicked:
                    {
                        if(txtCouponReceived.text === "Chỉnh sửa")
                        {
                            txtCouponReceived.text = "Lưu"
                            rootInfo.readOnly = false
                        }
                        else if(txtCouponReceived.text === "Lưu")
                        {
                            var name = tfsubName.text
                            var email = tfsubEmail.text
                            var dob = tfsubDOB.text
                            var sex = rdMale.checked ? 0 : 1

                            if(email.length > 0)
                            {
                                if(!isEmail(email))
                                {
                                   infoError.text = "*Sai email"
                                    return
                                }
                            }
                            if(dob.length > 0)
                            {
                                dob = fixDate(tfsubDOB.text)
                                if(!isDOB(dob))
                                {
                                    infoError.text = "*Sai ngày sinh"
                                    return
                                }
                            }
                            var url = "http://api.khuyenmai.re/api/v1/profile/changeProfile/?name=" + name + "&email=" + email + "&birthday=" + dob + "&gender=" + sex
                            post(url)
                            txtCouponReceived.text = "Chỉnh sửa"
                            infoError.text = ""
                            rootInfo.readOnly = true
                        }

                    }
                }

                Text
                {
                    id: txtCouponReceived
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 15 * app.dp
                    text: "Chỉnh sửa"
                    color: palette.colorWhite
                }


                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: 100 * app.dp
                            implicitHeight: 50 * app.dp
                            color: palette.color_follow
                            radius: 80
                        }
                    }
            }
            Button
            {
                anchors.rightMargin: 150 * app.dp
                anchors.right: parent.right
                anchors.top: parent.top
                height: parent.height
                MouseArea{
                    anchors.fill: parent
                    onClicked:
                    {
                        onChangePassword()
                    }
                }

                Text
                {
                    id: txtChangePassword
                    anchors.verticalCenter: parent.verticalCenter
                    anchors.horizontalCenter: parent.horizontalCenter
                    font.pixelSize: 15 * app.dp
                    text: "Đổi mật khẩu"
                    color: palette.colorWhite
                }


                style: ButtonStyle {
                        background: Rectangle {
                            implicitWidth: 100 * app.dp
                            implicitHeight: 50 * app.dp
                            color: palette.color_follow
                            radius: 80
                        }
                    }
            }
        }        
    }    
}

